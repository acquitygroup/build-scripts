namespace :wordpress do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  namespace :deploy do

    task :sync_config do
      run "rm -rf /tmp/esb; mkdir -p /tmp/esb;"
      run "cd /tmp/esb; svn checkout --non-interactive --username=#{acquity_username} --password=#{acquity_password} http://subversion.acq/ondemand/#{client_shortcode}/#{project_code}/wordpress/trunk/app "
      run "rsync -rav --delete --exclude \"*.svn*\" /drive1/applications/CARBON_HOME/repository/deployment/ /tmp/esb/app/repository/deployment/"  
      run "cd /tmp/esb/app; svn status | grep \\! | awk '{$1=\"\"; print \"\\\"\"$0\"\\\"\"}' | sed -e 's/^\" */\"/' | xargs svn delete; true;"
      run "cd /tmp/esb/app; svn add --force --non-interactive --username=#{acquity_username} --password=#{acquity_password} ./*"
      timevar = Time.now
      run "cd /tmp/esb/app; svn commit --non-interactive --username=#{acquity_username} --password=#{acquity_password} -m \"Dev Sync #{timevar}\""
    end
    
    task :create_release_path, :roles => :app do
      run "mkdir -p #{release_path}"
    end
 
    
    
    task :copy_wordpress, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../Packages/wordpress.zip", "#{deploy_to}/wordpress.zip"
    end


    task :deploy_wordpress, :roles => :app do
      run "unzip -o '#{deploy_to}/wordpress.zip' -d #{release_path}/"
    end

    task :setup_wordpress, :roles => :app do
       run "rsync -rav --delete '#{release_path}/repository/deployment/' '#{deploy_to}/repository/deployment/'"
       run "rm -f #{deploy_to}/revision.txt; cp #{release_path}/revision.txt #{deploy_to}/revision.txt"
    end

    task :verify_wordpress, :roles => :app do       
       run "cat #{deploy_to}/revision.txt"
    end
    
    task :stop_wordpress, :roles => :app, :on_error => :continue do
      #run "cd '#{deploy_to}/bin'; #{java_home_cmd}; ./tcserver.sh #{stage_name} stop"
      run "cd '#{deploy_to}/bin'; ./wso2server.sh stop"
    end

    task :restart_wordpress, :roles => :app do
      
      wordpress.deploy.stop_wordpress
      print "Sleeping 60 seconds.\n"
      sleep 60
      #run "cd '#{deploy_to}/current/wordpress/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} start"
      run "cd '#{deploy_to}/bin'; ./wso2server.sh start"
    end

        
  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
