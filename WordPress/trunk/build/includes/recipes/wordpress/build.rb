namespace :wordpress do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do

  
    task :install_base do
      dirname = File.absolute_path(File.dirname(__FILE__))
      puts dirname
      system "mkdir -p '#{dirname}/../../../../../wordpressBase'"
      system "mkdir -p '#{dirname}/../../../../../Packages'"
      path = "#{dirname}/../../../../../wordpressBase/#{wordpress_version}"
      if File.exists?("#{path}/wordpress") && File.directory?("#{path}/wordpress")
        puts "Base wordpress Install Exists"
      else
        puts "Copying Base wordpress Install"
        system "mkdir -p '#{path}'"
        #set(:acquity_username) { Capistrano::CLI.ui.ask("Acquity username: ") }
        #set(:acquity_password) { Capistrano::CLI.password_prompt("Acquity password: ") }   
        if (wordpress_edition == "enterprise")
          system "smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"wordpress/enterprise\";get wordpress-enterprise-#{wordpress_version}.zip \"#{path}/wordpress-enterprise-#{wordpress_version}.tar.gz\";exit' "
          system "cd '#{path}'; tar -zxvf wordpress-enterprise-#{wordpress_version}.tar.gz"
          system "rm -f '#{path}/wordpress-enterprise-#{wordpress_version}.tar.gz'"
        else
          system "smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"wordpress/community\";get wordpress-#{wordpress_version}.zip \"#{path}/wordpress-#{wordpress_version}.tar.gz\";exit' "
          system "cd '#{path}'; tar -zxvf wordpress-#{wordpress_version}.tar.gz"
          system "rm -f '#{path}/wordpress-#{wordpress_version}.tar.gz'"
        end
       
      end
      #system "cp -Rnv '#{path}/hybris/bin' '#{dirname}/../../../../' "
      #puts "mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      #system "[ -d '#{dirname}/../../../../config' ] && mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      #puts "cd '#{dirname}/../../../../bin/platform'; source setantenv.sh ; ant server -Dinput.template=develop"
      #system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant server -Dinput.template=develop"
      #puts "[ -d '#{dirname}/../../../../config-temp' ] && cp -Rnv '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      #system "[ -d '#{dirname}/../../../../config-temp' ] && cp -Rnv  '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      #system "[ -d '#{dirname}/../../../../config-temp' ] && rm -rf '#{dirname}/../../../../config' "
      #system "[ -d '#{dirname}/../../../../config-temp' ] && mv '#{dirname}/../../../../config-temp' '#{dirname}/../../../../config' "
    end
    
    
    task :build do      
      wordpress.build.install_revisiontxt
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "mkdir -p '#{dirname}/../../../../Packages'"      
      system "cd '#{dirname}/../../../../sources'; rm -f '#{dirname}/../../../../Packages/wordpress.zip'; zip -r -x='.svn' '#{dirname}/../../../../Packages/wordpress.zip' *"
    end

    task :install_revisiontxt do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "rm -f '#{dirname}/../../../../source/revision.txt'"
      system "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../app'/revision.txt"            
    end
    
  end
end