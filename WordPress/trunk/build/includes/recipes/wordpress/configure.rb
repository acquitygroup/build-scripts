namespace :wordpress do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :apache_vhost_conf_template, File.dirname(__FILE__) + "/apache_vhost_conf.erb"
  set :wordpress_createdb_template, File.dirname(__FILE__) + "/createdb.erb"
  set :wordpress_wpconfig_template, File.dirname(__FILE__) + "/wp-config.erb"
  namespace :configure do


    task :fix_sudoers_for_cap, roles => :app do
      run "sed -i 's/ requiretty/!requiretty/' /etc/sudoers; chmod 0440 /etc/sudoers"
      run "sed -i 's/ !visiblepw/visiblepw/' /etc/sudoers; chmod 0440 /etc/sudoers"
    end

    task :install_audit_tools, roles => :app do
      run "yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install policycoreutils-python"
    end

    task :set_hostname, roles => :app do
      if site_name == "wordpress.local"
        run "hostname #{site_name}"
        # run "echo -e '10.40.10.34 wordpress.local' >> /etc/hosts"
        # run "echo -e '10.40.10.36 master.wordpress.local' >> /etc/hosts"
      end
      if site_name == "wordpress.acq"
        # run "echo -e '10.50.30.12 wordpress.acq' >> /etc/hosts"
        # run "echo -e '10.50.30.13 master.wordpress.acq' >> /etc/hosts"
      end
    end

    task :install_svn_directories, roles => :app  do
      sudo "mkdir -p /usr/local/svn"
      sudo "mkdir -p /usr/local/svn/ondemand"
      sudo "mkdir -p /usr/local/svn/conf"
      sudo "mkdir -p /usr/local/svn/logs"
      sudo "groupadd -f svn"
      sudo "useradd -g svn -d /usr/local/svn svn"
      sudo "mkdir -p /usr/local/svn/.ssh;"
      run "touch /usr/local/svn/logs/svnsync.log"
      run "chown -R svn:svn /usr/local/svn"
      run "chown -R svn:svn /usr/local/svn/ondemand"
      run "chmod 700 /usr/local/svn/.ssh"
      #run "yum -y install httpd"
      run "yum -y update coreutils"
      run "chcon -R unconfined_u:object_r:httpd_sys_content_rw_t:s0 /usr/local/svn/ondemand"
      run "chcon -R unconfined_u:object_r:httpd_log_t:s0 /usr/local/svn/logs"
      run "chcon unconfined_u:object_r:user_home_dir_t:s0 /usr/local/svn"
      run "chcon unconfined_u:object_r:ssh_home_t:s0 /usr/local/svn/.ssh"
    end

    task :install_deploy_keys,roles => :app do
      timestamp = Time.now.to_i
      # Well, There is a centos bug if Selinux is on. http://bugs.centos.org/view.php?id=5432
      #sudo "setenforce permissive"
      #sudo "ssh-keygen -t rsa -f /usr/local/svn/.ssh/svn-#{timestamp} -P ''", :as => "svn"
      #sudo "cp /usr/local/svn/.ssh/svn-#{timestamp} /usr/local/svn/.ssh/id_rsa", :as => "svn"
      wordpress.configure.install_private_key
      wordpress.configure.install_public_key
      run "cp /usr/local/svn/.ssh/svn.pub /usr/local/svn/.ssh/authorized_keys"
      run "chown -R svn:svn /usr/local/svn/.ssh"
      run "chmod 600 /usr/local/svn/.ssh/id_rsa"
    end

    task :install_java, roles => :app do
      #run "yum -y install jdk"
      #run "wget -q -O /tmp/jdk-7-linux-x64.rpm http://download.oracle.com/otn-pub/java/jdk/7/jdk-7-linux-x64.rpm"
      #run "rpm -U --force /tmp/jdk-7-linux-x64.rpm"
    end

    task :install_tomcat,roles => :app  do
      run "wget -q -O /usr/local/apache-tomcat-#{tomcat_version}.tar.gz http://archive.apache.org/dist/tomcat/tomcat-7/v#{tomcat_version}/bin/apache-tomcat-#{tomcat_version}.tar.gz"
      run "cd /usr/local; tar -zxf apache-tomcat-#{tomcat_version}.tar.gz"
      run "mkdir -p /releng/master/tomcat; mkdir -p /releng/dev/tomcat; mkdir -p /releng/qa/tomcat; mkdir -p /releng/prod/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/master/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/dev/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/qa/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/prod/tomcat"
      run "mkdir -p /releng/master/tomcat/logs; mkdir -p /releng/dev/tomcat/logs; mkdir -p /releng/qa/tomcat/logs; mkdir -p /releng/prod/tomcat/logs"
      run "mkdir -p /releng/master/tomcat/temp; mkdir -p /releng/dev/tomcat/temp; mkdir -p /releng/qa/tomcat/temp; mkdir -p /releng/prod/tomcat/temp"
      run "mkdir -p /releng/master/tomcat/webapps; mkdir -p /releng/dev/tomcat/webapps; mkdir -p /releng/qa/tomcat/webapps; mkdir -p /releng/prod/tomcat/webapps"
      run "mkdir -p /releng/master/tomcat/work; mkdir -p /releng/dev/tomcat/work; mkdir -p /releng/qa/tomcat/work; mkdir -p /releng/prod/tomcat/work"
      run "chown -R deploy-master:deploy-master /releng/master"
      run "chown -R deploy-dev:deploy-dev /releng/dev"
      run "chown -R deploy-qa:deploy-qa /releng/qa"
      run "chown -R deploy-prod:deploy-prod /releng/prod"
      stages.each { |stage|
        set :stage_name, stage
        wordpress.configure.install_server_script
        wordpress.configure.install_server_xml
      }
    end
    
    task :install_php, roles => :app do
      if Gem::Version.new(sys_version) < Gem::Version.new('6.0')
        # install Webtatic Repo
        run "#{sudo} rpm -Uvh http://repo.webtatic.com/yum/centos/5/latest.rpm; true"
        #Enable just first webtatic repo
        run "#{sudo} sed -i '0,/enabled/{s/enabled=0/enabled=1/}' /etc/yum.repos.d/webtatic.repo"
        
        run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install php php-devel php-common php-pecl-memcache php-ldap php-pecl-apc php-pecl-xdebug php-pdo php-mhash php-mcrypt php-fpm php-mysql php-xml php-suhosin php-process"
      else
        #install Webtatic Repo
        run "#{sudo} rpm -Uvh http://repo.webtatic.com/yum/el6/latest.rpm; true"
        #Enable just first webtatic repo
        run "#{sudo} sed -i '0,/enabled/{s/enabled=0/enabled=1/}' /etc/yum.repos.d/webtatic.repo"    
        run "#{sudo} yum -y remove php*"
        run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install php54w php54w-devel php54w-common php54w-pecl-memcache php54w-ldap php54w-pecl-apc php54w-pecl-xdebug php54w-pdo php54w-gd php54w-mcrypt php54w-mhash php54w-fpm php54w-mysql php54w-xml php54w-suhosin php54w-process"
      end
    end
    
    
    
    task :install_newrelic, roles => :app do
      run "#{sudo} rpm -Uvh http://yum.newrelic.com/pub/newrelic/el5/x86_64/newrelic-repo-5-3.noarch.rpm; true"
      run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install newrelic-php5"
      run "#{sudo} NR_INSTALL_SILENT=true newrelic-install install"
    end
    

    task :install_wordpress, roles => :app do
      path = "#{deploy_to}/releases/1/"
      run "mkdir -p '#{path}'"
      run "chmod +w '#{path}'"
      run "chcon -R unconfined_u:object_r:httpd_sys_content_rw_t:s0 #{deploy_to}"
      #set(:acquity_username) { Capistrano::CLI.ui.ask("Acquity username: ") }
      #set(:acquity_password) { Capistrano::CLI.password_prompt("Acquity password: ") }   
        
      run "cd '#{path}'; wget http://wordpress.org/wordpress-#{wordpress_version}.tar.gz;exit "
      run "cd '#{path}'; tar -zxvf wordpress-#{wordpress_version}.tar.gz; true"
      run "rm -f '#{path}/wordpress-#{wordpress_version}.tar.gz'"
        
      #run "cd '#{path}'/wordpress; if [ ! -d '#{deploy_to}/media' ]; then mv media '#{deploy_to}'; fi ", { :shell => 'bash'}
      #run "cd '#{path}'/wordpress; rm -rf media";
      #run "ln -sf '#{deploy_to}/media' '#{path}/wordpress/media' "
      #run "cd '#{path}'/wordpress; if [ ! -d '#{deploy_to}/var' ]; then mv var '#{deploy_to}'; fi ", { :shell => 'bash'}
      #run "cd '#{path}'/wordpress; rm -rf var";
      #run "ln -sf '#{deploy_to}/var' '#{path}/wordpress/var'"
      run "ln -sf '#{path}' '#{deploy_to}/current'"
      run "chown -R apache.apache '#{deploy_to}'"
      #run "cd /tmp; tar -zxf wordpress-#{wordpress_version}.tar.gz"
      #run "cd /tmp/wordpress-#{wordpress_version}; ./configure --libdir=/usr/lib64"
      #run "cd /tmp/wordpress-#{wordpress_version}; make"
      #run "cd /tmp/wordpress-#{wordpress_version}; make check"
      #run "cd /tmp/wordpress-#{wordpress_version}; make install"
      #run "cd /tmp/wordpress-#{wordpress_version}; make install-swig-py"
      #run "echo \"/usr/lib64/svn-python\" > /usr/lib64/python2.6/site-packages/svn.pth"
      #run "cd /usr/local/svn/ondemand; #{sudo} -u svn /usr/local/bin/svnadmin create Hybris"
      wordpress.configure.install_wpconfig
      run "cd '#{path}/wordpress'; /root/.composer/bin/wp core install --url=#{site_name} --title='Wordpress #{stage_name}' --admin_name=admin --admin_email=operations@acquitygroup.com --admin_password=admin; true "
      run "cd '#{path}/wordpress'; /root/.composer/bin/wp post delete 1"
    end

    task :install_wpcli, :roles => :app do
      run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install git"
      run "curl http://wp-cli.org/installer.sh | bash"
    end
    
    task :install_wpconfig, :roles => :app do
       set :db_password, fetch(:db_password, "wordpress")
       install_template wordpress_wpconfig_template, "#{deploy_to}/releases/1/wordpress/wp-config.php"
    end
    
    task :install_mysql, :roles => :db do
      run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install mysql mysql-server"
      run "#{sudo} chkconfig --level 5 mysqld on" 
      run "mkdir -p '#{deploy_to}'" 
      wordpress.configure.install_createdb
          
    end
    
    task :configure_mysql, :roles => :db do   
      
      run "#{sudo} mysql -fv < #{deploy_to}/createdb.sql"      
    end
    
    task :restart_mysql, :roles => :db do
      run "service mysqld restart"
    end
    
     task :install_createdb, :roles => :app, :except => { :no_release => true } do
      set :db_password, fetch(:db_password, "wordpress")
      install_template wordpress_createdb_template, "#{deploy_to}/createdb.sql"
    end

    
    task :install_viewvc, roles => :app do
      run "yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install dos2unix python-pygments"
      run "wget -q -O /tmp/viewvc-1.1.13.zip http://viewvc.tigris.org/files/documents/3330/49162/viewvc-1.1.13.zip"
      run "cd /tmp; unzip -o viewvc-1.1.13.zip"
      run "dos2unix /tmp/viewvc-1.1.13/viewvc-install"
      run "/tmp/viewvc-1.1.13/viewvc-install --prefix=/usr/local/viewvc-1.1.13 --destdir="
      run "sed -i 's/#root_parents =/root_parents = \\/usr\\/local\\/svn\\/ondemand: svn/' /usr/local/viewvc-1.1.13/viewvc.conf"
      run "chcon -R unconfined_u:object_r:httpd_sys_script_rw_t:s0 /usr/local/viewvc-1.1.13"
    end

    task :sync_svn_conf, roles => :app do
      run "#{sudo} -u svn ssh -o 'StrictHostKeyChecking no' tesla.acquityondemand.com -l svn exit"
      run "#{sudo} -u svn rsync -r svn@tesla.acquityondemand.com:/usr/local/svn/conf/* /usr/local/svn/conf"
      run "htpasswd -bc /usr/local/svn/conf/repos-sync-user svnsync svnsync321"
    end
    
    task :install_cron_scripts, roles => :app do
      run "mkdir -p /usr/local/svn/cron/master"
      run "mkdir -p /usr/local/svn/cron/slave"
      wordpress.configure.install_master_cron
      wordpress.configure.install_slave_cron
      run "chmod +x /usr/local/svn/cron/master/*.sh"
      run "chmod +x /usr/local/svn/cron/slave/*.sh"
      run "chown -R svn.svn /usr/local/svn/cron"
    end
    
    task :install_global_hooks, roles => :app do
      run "mkdir -p /usr/local/svn/hooks/master"
      run "mkdir -p /usr/local/svn/hooks/slave"
      run "mkdir -p /usr/local/svn/hooks/template"
      wordpress.configure.install_slave_repository_hooks
      wordpress.configure.install_master_repository_hooks
      wordpress.configure.install_template_repository_hooks
      run "chmod +x /usr/local/svn/hooks/master/*"
      run "chmod +x /usr/local/svn/hooks/slave/*"
      run "chmod +x /usr/local/svn/hooks/template/*"
      run "chown -R svn.svn /usr/local/svn/hooks"
    end

    task :install_slave_symlinks, roles => :app, :only => { :slave => true } do
      run "mkdir -p /usr/local/svn/current"
      run "ln -sf /usr/local/svn/cron/slave /usr/local/svn/current/cron"
      run "ln -sf /usr/local/svn/hooks/slave /usr/local/svn/current/hooks"
      run "chown -R svn.svn /usr/local/svn/current"
    end

    task :install_master_symlinks, roles => :app, :only => { :master => true } do
      run "mkdir -p /usr/local/svn/current"
      run "ln -sf /usr/local/svn/cron/master /usr/local/svn/current/cron"
      run "ln -sf /usr/local/svn/hooks/master /usr/local/svn/current/hooks"
      run "chown -R svn.svn /usr/local/svn/current"
    end

    task :start_cron, roles => :app do
      run "#{sudo} -u svn ssh -o 'StrictHostKeyChecking no' master.#{site_name} -l svn exit"
      run "#{sudo} -u svn ssh -o 'StrictHostKeyChecking no' #{site_name} -l svn exit"
      run "#{sudo} -u svn crontab /usr/local/svn/current/cron/crontab"
    end

    task :install_local_slave_repository_hooks, roles => :app, :only => { :slave => true } do
      run "#{sudo} su - svn -c \"cp -u /usr/local/svn/hooks/template/* /usr/local/svn/ondemand/#{repo}/hooks/\""
      run "#{sudo} su - svn -c \"chcon -R unconfined_u:object_r:httpd_unconfined_script_exec_t:s0 /usr/local/svn/ondemand/#{repo}/hooks/*\""
    end

    task :install_local_master_repository_hooks, roles => :app, :only => { :master => true } do
      run "#{sudo} su - svn -c \"cp -u /usr/local/svn/hooks/template/* /usr/local/svn/ondemand/#{repo}/hooks/\""
      run "#{sudo} su - svn -c \"chcon -R unconfined_u:object_r:httpd_unconfined_script_exec_t:s0 /usr/local/svn/ondemand/#{repo}/hooks/*\""
    end


    task :populate_master_repositories, roles => :app, :only => { :master => true } do
      run "#{sudo} -u svn scp svn@tesla.acquityondemand.com:/home/svn/wordpress_dump.gz /usr/local/svn/wordpress_dump.gz"
      run "gunzip /usr/local/svn/wordpress_dump.gz"
      run "#{sudo} -u svn /usr/local/bin/svnadmin load --force-uuid /usr/local/svn/ondemand/Hybris < /usr/local/svn/wordpress_dump"
    end

    task :sync_slave_repositories, roles => :app, :only => { :slave => true } do
      run "#{sudo} su - svn -c \"/usr/local/bin/svnsync initialize http://#{site_name}/repos-sync/ondemand/#{repo} http://master.#{site_name}/repos-sync/ondemand/#{repo} --sync-username svnsync --sync-password svnsync321 --source-username svnsync --source-password svnsync321 --non-interactive\""
      run "#{sudo} su - svn -c \"/usr/local/bin/svnsync sync http://#{site_name}/repos-sync/ondemand/#{repo} --sync-username svnsync --sync-password svnsync321 --source-username svnsync --source-password svnsync321 --non-interactive\""
    end

    
    task :start_http_server, roles => :app do
      run "service httpd restart"
    end

    task :install_fuse, roles => :app do
      run "yum -y install http://pkgs.repoforge.org/fuse-sshfs/fuse-sshfs-2.2-1.el6.rf.x86_64.rpm"
      run "#{sudo} -u svn /bin/mkdir -p /usr/local/svn/tesla"
      run "usermod -G fuse svn"
    end

    task :mount_fuse, roles => :app, :only => { :master => true } do
      run "#{sudo} -u svn sshfs -o ro tesla.acquityondemand.com:/usr/local/svn /usr/local/svn/tesla"
    end

    task :create_repository, roles => :app do
      
      set(:repo,  Capistrano::CLI.ui.ask("Repository: ") )
      set(:source_repo_dir, Capistrano::CLI.ui.ask("Source UUID Repository Dir (leave blank for none): ") )
      if (!source_repo_dir.empty?)
        uuid = capture("#{sudo} su - svn -c \"svnlook uuid /usr/local/svn/tesla/projects/#{source_repo_dir}\"",:only => { :master => true })
      else
        uuid = ""
      end
      print uuid
      run "#{sudo} su - svn -c \"svnadmin create /usr/local/svn/ondemand/#{repo}\" "
      run "#{sudo} su - svn -c \"svnadmin setuuid /usr/local/svn/ondemand/#{repo}\ #{uuid}\" "
      run "#{sudo} su - svn -c \"rm -rf /usr/local/svn/ondemand/#{repo}/conf/*\""
      run "#{sudo} su - svn -c \"ln -s /usr/local/svn/conf/authz /usr/local/svn/ondemand/#{repo}/conf/authz\""
      run "#{sudo} su - svn -c \"ln -s /usr/local/svn/conf/passwd /usr/local/svn/ondemand/#{repo}/conf/passwd\""
      run "#{sudo} su - svn -c \"ln -s /usr/local/svn/conf/svnserve.conf /usr/local/svn/ondemand/#{repo}/conf/svnserve.conf\""
      wordpress.configure.install_local_slave_repository_hooks
      wordpress.configure.sync_slave_repositories
      wordpress.configure.install_local_master_repository_hooks
    end

    task :delete_repository, roles => :app do

      set(:repo,  Capistrano::CLI.ui.ask("Repository: ") )
      run "#{sudo} su - svn -c \"rm -rf /usr/local/svn/ondemand/#{repo}*\""
    end

    task :import_repository, roles => :app, :only => { :master => true } do
      set(:repo, Capistrano::CLI.ui.ask("Source Repository: ") )
      set(:dest_repo, Capistrano::CLI.ui.ask("Destination Repository: ") )
      #set(:filter_dir, Capistrano::CLI.ui.ask("Repository Directory Filter: ") )
      run "#{sudo} su - svn -c \"svnadmin dump /usr/local/svn/tesla/projects/#{repo} > /usr/local/svn/#{repo}.svndump \""
      #run "#{sudo} su - svn -c \"svndumpfilter include --pattern '*#{filter_dir}' < /usr/local/svn/#{repo}.svndump > /usr/local/svn/#{dest_repo}.svndump \""
      #run "#{sudo} su - svn -c \"svnadmin load /usr/local/svn/ondemand/#{dest_repo} < /usr/local/svn/#{repo}.svndump \""
    end

 

    task :install_apache, roles => :app  do
      run "yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install httpd"
      #run "mkdir -p //logs"
      #run "chown -R apache.apache /www/"
      #run "chcon -R unconfined_u:object_r:httpd_log_t:s0 /www/logs"
      #wordpress.configure.install_httpd_conf
      wordpress.configure.install_apache_conf
      #wordpress.configure.install_default_site
      run "touch /var/www/html/favicon.ico"
      run "chkconfig --level 5 httpd on"
      #run "iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT"
      #run "setenforce 1"
      run "if [[ ! `/usr/sbin/sestatus | grep status` == *disabled* ]]; then /usr/sbin/setsebool -P httpd_can_network_connect 1; fi", { :shell => 'bash'}
      run "if [[ ! `/usr/sbin/sestatus | grep status` == *disabled* ]]; then /usr/sbin/setsebool -P httpd_read_user_content 1; fi", { :shell => 'bash'}
      run "if [[ ! `/usr/sbin/sestatus | grep status` == *disabled* ]]; then /usr/sbin/setsebool -P httpd_enable_homedirs 1; fi", { :shell => 'bash'}      
      run "iptables -A INPUT -p tcp --dport 80 -j ACCEPT"
      run "iptables -A INPUT -p tcp --dport 443 -j ACCEPT"
      run "service iptables save"
      run "service iptables restart"
    end

    task :install_private_key, roles => :web, :except => { :no_release => true } do
      template = File.read("#{private_key_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/.ssh/id_rsa"
    end
    
    task :install_public_key, roles => :web, :except => { :no_release => true } do
      template = File.read("#{public_key_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/.ssh/svn.pub"
    end

    task :install_slave_repository_hooks, roles => :app do
      template = File.read("#{svn_slave_pre_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/pre-revprop-change"
      template = File.read("#{svn_slave_start_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/start-commit"
      template = File.read("#{svn_slave_post_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/post-revprop-change"
      template = File.read("#{svn_slave_post_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/post-commit"
    end

    task :install_template_repository_hooks, roles => :app do
      template = File.read("#{svn_local_pre_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/pre-revprop-change"
      template = File.read("#{svn_local_start_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/start-commit"
      template = File.read("#{svn_local_post_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/post-revprop-change"
      template = File.read("#{svn_local_post_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/post-commit"
    end

    task :install_master_repository_hooks, roles => :app  do
      template = File.read("#{svn_master_post_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/post-revprop-change"
      template = File.read("#{svn_master_post_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/post-commit"
      template = File.read("#{svn_master_pre_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/pre-revprop-change"
      template = File.read("#{svn_master_start_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/start-commit"
    end

    task :install_master_cron, roles => :app  do
      template = File.read("#{cron_master_crontab_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/master/crontab"
      template = File.read("#{cron_master_remount_tesla_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/master/remount_tesla.sh"
      template = File.read("#{cron_master_sync_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/master/sync_conf.sh"
    end

    task :install_slave_cron, roles => :app  do
      template = File.read("#{cron_slave_crontab_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/slave/crontab"
      template = File.read("#{cron_slave_sync_repos_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/slave/sync_repos.sh"
      template = File.read("#{cron_slave_sync_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/slave/sync_conf.sh"
    end


    task :install_httpd_conf, roles => :web, :except => { :no_release => true } do
      template = File.read("#{apache_httpd_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf/httpd.conf"
    end

    task :install_apache_conf, roles => :web, :except => { :no_release => true } do
      template = File.read("#{apache_vhost_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf.d/#{site_name}.conf"  
    end

    task :install_default_site, roles => :web, :except => { :no_release => true } do
      template = File.read("#{default_site_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/var/www/html/index.html"
    end
    
     def install_template(template_name,location)
      template = File.read("#{template_name}")
      buffer = ERB.new(template).result(binding)
      put buffer, location
      run "/bin/sed -i 's/HOSTNAME/$CAPISTRANO:HOST$/' #{location}"
    end

  end
end 
