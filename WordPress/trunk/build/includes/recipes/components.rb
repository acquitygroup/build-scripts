set :application, "wordpress"
set :tomcatports, { "master" => 9000,
  "dev" => 9001,
  "qa" => 9002,
  "prod" => 9003  
}
set :stages, [ "master", "dev", "qa", "prod" ]