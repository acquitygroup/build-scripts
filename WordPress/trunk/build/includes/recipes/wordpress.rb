load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/wordpress/configure'
load File.dirname(__FILE__) + '/wordpress/build'
load File.dirname(__FILE__) + '/wordpress/deploy'
load File.dirname(__FILE__) + '/wordpress/util'


namespace :wordpress do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 