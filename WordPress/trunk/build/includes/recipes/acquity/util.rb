namespace :acquity do


  namespace :util do
   
      task :start_vpn do
        print "Checking for running VPN to #{vpn_datacenter}.\n"
        vpncheck = `ps -ef | grep vpnc | grep root`
        if vpncheck.include? "vpnc #{vpn_datacenter}" then
          print "VPN to #{vpn_datacenter} Running, skipping."
        else
          print "Starting VPN to #{vpn_datacenter}.\n"
           system "sudo /usr/local/sbin/vpnc #{vpn_datacenter}"
           if $?.exitstatus != 0 then
              raise "VPN Start Failure!"
           end
        end
      end

      task :stop_vpn do
        print "Stopping VPN.\n"
        system "sudo /usr/local/sbin/vpnc-disconnect"
        if $?.exitstatus != 0 then
        raise "VPN Stop Failure!"
        end
      end

     task :detect_os_version, :roles => :app do
      sys_version = capture "grep -Eo '[[:digit:].]+' /etc/redhat-release | awk -F. '{print $1\".\"$2}'"
      set :sys_version, sys_version
      if Gem::Version.new(sys_version) < Gem::Version.new('6.0')
        set :selinux_user, "root"
      else
        set :selinux_user, "undefined_u"
      end
    end
    
  end
end