default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    acquity.util.detect_os_version
    update
    #restart
  end

  task :build do
    wordpress.build.build
  end

  task :cold do
    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    setup
    start
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end


  task :restart, :except => { :no_release => true } do
    wordpress.deploy.restart_wordpress
  end


   task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    wordpress.deploy.create_release_path
    wordpress.deploy.copy_wordpress    
    finalize_update
  end

 
  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update do
    wordpress.deploy.deploy_wordpress
    wordpress.deploy.setup_wordpress
    wordpress.deploy.verify_wordpress
    cleanup      
   end

  task :setup do   
    #set :user, "root"
    acquity.util.detect_os_version
    wordpress.configure.fix_sudoers_for_cap
    wordpress.configure.install_audit_tools
    #wordpress.configure.set_hostname
    #wordpress.configure.install_svn_directories
    wordpress.configure.install_apache
    wordpress.configure.install_php
    if fetch(:use_new_relic, false) == true then
      wordpress.configure.install_newrelic
    end
    if fetch(:skip_db_install, false) == false then
      wordpress.configure.install_mysql            
    end      
    wordpress.configure.restart_mysql
    wordpress.configure.configure_mysql
    wordpress.configure.install_wpcli
    wordpress.configure.install_wordpress
    #wordpress.configure.install_deploy_keys
    #wordpress.configure.install_viewvc
    #wordpress.configure.sync_svn_conf
    #wordpress.configure.install_fuse
    #wordpress.configure.mount_fuse
    #wordpress.configure.start_http_server
    #wordpress.configure.install_cron_scripts
    #wordpress.configure.install_global_hooks
    #wordpress.configure.install_master_symlinks
    #wordpress.configure.install_slave_symlinks
    #wordpress.configure.start_cron
  end

  task :start do
    wordpress.configure.start_http_server
  end


end

after 'deploy:restart', "deploy:after_restart"