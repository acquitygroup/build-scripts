desc "deploy to master environment"
task :master do
  set :stage_name, "master"
end

desc "deploy to VM environment"
task :VM do
  set :stage_name, "vm"
  set :site_name, "wordpress.local"
  set :use_new_relic, true
  set :skip_db_install, false
  set :user, "root"
  set :deploy_to, "/usr/applications/ag-commerce-acq-dev"
  role :app, "10.21.13.116"
  role :web, "10.21.13.116"
  role :db, "10.21.13.116"
end



desc "deploy to dev environment"
task :dev do
  set :stage_name, "dev"
  set :site_name, "dev.acq.acq"
  set :username, "acq"
  set :deploy_to, "/usr/applications/ag-commerce-acq-dev"
  role :app, "10.12.40.17"
  role :web, "10.12.40.17"
end



desc "deploy to qa environment"
task :qa do
  set :stage_name, "qa"
  set :site_name, "qa.acq.acq"
  set :username, "acq"
  set :deploy_to, "/usr/applications/ag-commerce-acq-qa"
  role :app, "10.50.40.60"
  role :app, "10.50.40.61"
  role :web, "10.50.40.60"
  role :web, "10.50.40.61"
end

desc "UAT Environment"
task :UAT do
  set :stage_name, "uat"
  set :site_name, "uat.acq.acq"
  #role :app, "172.26.48.58"
  #role :web, "172.26.48.58"
end

desc "deploy to prod environment"
task :prod do
  set :stage_name, "prod"

end