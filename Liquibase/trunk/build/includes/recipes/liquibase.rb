load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/liquibase/configure'
load File.dirname(__FILE__) + '/liquibase/build'
load File.dirname(__FILE__) + '/liquibase/deploy'
load File.dirname(__FILE__) + '/liquibase/util'
set :tomcat_version, "7.0.27"

namespace :liquibase do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 