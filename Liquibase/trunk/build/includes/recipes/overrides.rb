default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    update
    restart
  end

  task :build do
    liquibase.build.build
  end

  task :cold do
    setup
    start
  end


  task :restart, :except => { :no_release => true } do
    liquibase.deploy.update_liquibase
  end


   task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    liquibase.deploy.create_release_path
    liquibase.deploy.copy_liquibase    
    finalize_update
  end

 
  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update do
    liquibase.deploy.deploy_liquibase
    liquibase.deploy.setup_liquibase
    liquibase.deploy.verify_liquibase
    cleanup      
   end

  task :setup do
    set :user, "root"
    #liquibase.configure.fix_sudoers_for_cap
    #liquibase.configure.install_audit_tools
    #liquibase.configure.set_hostname
    #liquibase.configure.install_svn_directories
    #liquibase.configure.install_apache
    #liquibase.configure.install_liquibase
    #liquibase.configure.install_deploy_keys
    #liquibase.configure.install_viewvc
    #liquibase.configure.sync_svn_conf
    #liquibase.configure.install_fuse
    #liquibase.configure.mount_fuse
    #liquibase.configure.start_http_server
    #liquibase.configure.install_cron_scripts
    #liquibase.configure.install_global_hooks
    #liquibase.configure.install_master_symlinks
    #liquibase.configure.install_slave_symlinks
    #liquibase.configure.start_cron
  end

  task :start do
    #liquibase.configure.start_http_server
  end
  namespace :rollback do
 
    task :default do
      liquibase.deploy.rollback_liquibase
      revision
      cleanup
    end
end
  
end

after 'deploy:restart', "deploy:after_restart"