
namespace :liquibase do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  namespace :deploy do

    task :sync_config do
      run "rm -rf /tmp/esb; mkdir -p /tmp/esb;"
      run "cd /tmp/esb; svn checkout --non-interactive --username=#{acquity_username} --password=#{acquity_password} http://subversion.acq/ondemand/#{client_shortcode}/#{project_code}/liquibase/trunk/app "
      run "rsync -rav --delete --exclude \"*.svn*\" /drive1/applications/CARBON_HOME/repository/deployment/ /tmp/esb/app/repository/deployment/"  
      run "cd /tmp/esb/app; svn status | grep \\! | awk '{$1=\"\"; print \"\\\"\"$0\"\\\"\"}' | sed -e 's/^\" */\"/' | xargs svn delete; true;"
      run "cd /tmp/esb/app; svn add --force --non-interactive --username=#{acquity_username} --password=#{acquity_password} ./*"
      timevar = Time.now
      run "cd /tmp/esb/app; svn commit --non-interactive --username=#{acquity_username} --password=#{acquity_password} -m \"Dev Sync #{timevar}\""
    end
    
    task :create_release_path, :roles => :app do
      run "mkdir -p #{release_path}"
    end

    task :detect_java_home, :roles => :app do
      if remote_file_exists?("/usr/applications/java/current") then
        java_home = "/usr/applications/java/current"
      elsif remote_file_exists?("/usr/java/latest") then
        java_home = "/usr/java/latest"
      else
        java_home = "/usr/applications/#{jdk_version}"
      end
      set :java_home, "#{java_home}"
      set :java_home_cmd, "export JAVA_HOME='#{java_home}'"
    end
    
    
    task :copy_liquibase, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../Packages/Liquibase.zip", "#{deploy_to}/Liquibase.zip"
    end


    task :deploy_liquibase, :roles => :app do
      run "unzip -o '#{deploy_to}/Liquibase.zip' -d #{release_path}/"      
    end

    task :initialize_liquibase, :roles => :app do            
      #config_rev = capture("grep 'Changed Rev' #{deploy_to}/current/revision.txt | cut -c19-").strip      
      #print "Replacing Changed Revision: #{config_rev} with ALPHA in tag.xml file.\n"
      #run "sed -i 's/#{config_rev}/ALPHA/g' #{deploy_to}/current/tag.xml"
      #liquibase.deploy.update_liquibase
      #print "Injecting Changed Revision: #{config_rev} in tag.xml file.\n"
      #run "sed -i 's/@REV@/#{config_rev}/g' #{deploy_to}/current/tag.xml"
      puts "This is destructive! Nuking Deploy_To!"
      run "rm -rf #{deploy_to}" 
      puts "Calling deploy:update"
      find_and_execute_task("deploy:update")
      run "cd '#{deploy_to}'/current; '#{deploy_to}/current/liquibase/liquibase'  --logLevel=debug --changeLogFile='#{deploy_to}'/current/initialize.xml --defaultsFile='#{deploy_to}'/current/config/liquibase_#{stage_name}.properties update"
    end
    
     task :rollback_liquibase, :roles => :app do       
      rollback_rev = capture("cd '#{deploy_to}/current'; cat rollback") 
      puts "Creating rollback SQL script for #{rollback_rev} ..." 
      run "cd '#{deploy_to}'/current; '#{deploy_to}/current/liquibase/liquibase'  --logLevel=debug --changeLogFile='#{deploy_to}'/current/update.xml --defaultsFile='#{deploy_to}'/current/config/liquibase_#{stage_name}.properties rollbackSQL #{rollback_rev} > #{deploy_to}/current/rollback.sql"
      puts "Fetching rollback SQL script..."    
      workspace = File.expand_path(File.dirname(__FILE__)) + "/../../../../.."
      if ENV['WORKSPACE'] then
        workspace = ENV['WORKSPACE']
      end    
      download("#{deploy_to}/current/rollback.sql","#{workspace}/rollback-#{rollback_rev}.sql", :once => true)
      run "cd '#{deploy_to}'/current; '#{deploy_to}/current/liquibase/liquibase'  --logLevel=debug --changeLogFile='#{deploy_to}'/current/update.xml --defaultsFile='#{deploy_to}'/current/config/liquibase_#{stage_name}.properties rollback #{rollback_rev}"
    end
   
    
    task :setup_liquibase, :roles => :app do
      run "chmod +x '#{release_path}/liquibase/liquibase' "
      config_rev = capture("grep 'Changed Rev' #{release_path}/revision.txt | cut -c19-").strip      
      print "Injecting Changed Revision: #{config_rev} in tag.xml file.\n"
      run "sed -i 's/@REV@/#{config_rev}/g' #{release_path}/tag.xml"
      if exists?(:db_password) then
        run "sed -i 's/DB_PASSWORD/#{db_password}/g' '#{release_path}'/config/liquibase_#{stage_name}.properties"
      end
      run "ln -s  '#{release_path}'/config/liquibase_#{stage_name}.properties '#{release_path}'/liquibase.properties"      
      if remote_file_exists?("#{deploy_to}/current/tag.xml")
        old_config_rev = capture("grep 'Changed Rev' #{deploy_to}/current/revision.txt | cut -c19-").strip 
        run "echo #{old_config_rev} > #{release_path}/rollback"
      else
        run "echo ALPHA > #{release_path}/rollback"
      end
    
    #run "rm -f #{deploy_to}/revision.txt; cp #{release_path}/revision.txt #{deploy_to}/revision.txt"
  end

  task :update_liquibase, :roles => :app do
    puts "Generating SQL at #{release_path}/changes.sql"
    run "cd '#{deploy_to}'/current; '#{deploy_to}/current/liquibase/liquibase'  --logLevel=debug --changeLogFile='#{deploy_to}'/current/update.xml --defaultsFile='#{deploy_to}'/current/config/liquibase_#{stage_name}.properties updateSQL > #{release_path}/changes.sql"
    puts "Fetching SQL script..."    
    workspace = File.expand_path(File.dirname(__FILE__)) + "/../../../../.."
    if ENV['WORKSPACE'] then
        workspace = ENV['WORKSPACE']
    end    
    download("#{release_path}/changes.sql","#{workspace}/changes-#{release_name}.sql", :once => true)
    puts "Executing Update"
    run "cd '#{deploy_to}'/current; '#{deploy_to}/current/liquibase/liquibase'  --logLevel=debug --changeLogFile='#{deploy_to}'/current/update.xml --defaultsFile='#{deploy_to}'/current/config/liquibase_#{stage_name}.properties update"
  end
    
  task :verify_liquibase, :roles => :app do       
    run "cat #{release_path}/revision.txt"
  end
    
  task :stop_liquibase, :roles => :app, :on_error => :continue do
    #run "cd '#{deploy_to}/bin'; #{java_home_cmd}; ./tcserver.sh #{stage_name} stop"
    run "cd '#{deploy_to}/bin'; ./wso2server.sh stop"
  end

  task :restart_liquibase, :roles => :app do
      
    liquibase.deploy.stop_liquibase
    #print "Sleeping 60 seconds.\n"
    #sleep 60
    #run "cd '#{deploy_to}/current/liquibase/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} start"
    #run "cd '#{deploy_to}/bin'; ./wso2server.sh start"
  end

        
end
end

def remote_file_exists?(full_path)
'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
