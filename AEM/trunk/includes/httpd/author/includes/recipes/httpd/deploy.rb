namespace :httpd do

  set :httpd_path, "/usr/sbin/httpd"
  set :conf_yaml, "config/conf.yaml"
  set :stage_name, "master"

  namespace :deploy do

    task :verify do
      run "cat '#{release_path}/revision.txt'"
    end

    task :stop, :on_error => :continue do
      run "#{service_command} -k stop"
    end

    task :restart, :on_error => :rollback do
      stop
      run "#{service_command} -k restart"
    end

    task :after_extract do
      generate_conf
    end

    task :generate_conf do
      conf = YAML.load(File.read "#{conf_yaml}")

      tmpls_default = {}
      incls_default = {}
      vhost_default = {}

      if conf["default"] then
        if conf["default"]["templates"] then
          tmpls_default = conf["default"]["templates"]
        end
        if conf["default"]["includes"] then
          incls_default = conf["default"]["includes"]
        end
        if conf["default"]["vhost"] then
          vhost_default = conf["default"]["vhost"]
        end
      end

      if conf["stages"] && conf["stages"]["#{stage_name}"] then
        conf_stage = conf["stages"]["#{stage_name}"]
        if conf_stage["templates"] then
          tmpls_default = conf_stage["templates"]
        end
        if conf_stage["includes"] then
          incls_default = conf_stage["includes"]
        end
        if conf_stage["vhost"] then
          vhost_default = conf_stage["vhost"]
        end
      end

      servers = find_servers_for_task(current_task)
      servers.each do |server|
        tmpls = tmpls_default
        incls = incls_default
        vhost = vhost_default

        if conf["servers"] && conf["servers"]["#{server}"] then
          server_conf = conf["servers"]["#{server}"]
          if server_conf["templates"] then
            tmpls = server_conf["templates"]
          end
          if server_conf["includes"] then
            incls = server_conf["includes"]
          end
          if server_conf["vhost"] then
            vhost = server_conf["vhost"]
          end
        end

        tmpls.each do |tmpl_name,file_name|
          eval_tmpl(tmpl_name, file_name, server, vhost, incls)
        end
      end
    end

    def service_command()
      "[ ! -f '#{httpd_conf_path}' ] || #{httpd_path} -d '#{httpd_base_path}' -f '#{httpd_conf_path}'"
    end

    def httpd_conf_path()
      "#{deploy_to}/current/httpd.conf"
    end

    def httpd_base_path()
      "#{File.dirname(deploy_to)}"
    end

    def eval_tmpl(tmpl_name, file_name, server, vhost = {}, includes = {})
      template = load_tmpl(tmpl_name)
      fn = "#{output.dir}/#{server}.#{file_name}"
      buffer = ERB.new(template).result(binding)
      File.open(fn, "w") { |f| f.write(buffer) }
      if !fetch(:only_build, false) then
        upload fn, "#{release_path}/#{file_name}", { :hosts => server, :via => :scp }
      end
    end

    def load_tmpl(tmpl_name)
      inc_httpd = File.dirname(__FILE__) 
      inc_tmpl = "#{inc_httpd}/tmpl"
      tmpl_dir = "src/tmpl"

      if File.exist?("#{tmpl_dir}/#{tmpl_name}") then
        File.read("#{tmpl_dir}/#{tmpl_name}")
      else
        File.read("#{inc_tmpl}/#{tmpl_name}")
      end
    end

  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
