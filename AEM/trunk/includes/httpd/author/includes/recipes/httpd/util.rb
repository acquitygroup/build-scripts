namespace :httpd do

  namespace :util do

    task :flush, :roles => [:www_author, :www] do
      conf = YAML.load(File.read "#{conf_yaml}")

      docroot_default = false
      flush_cmd_default = false

      if conf["default"] then
        if conf["default"]["vhost"] then
          if conf["default"]["vhost"]["flush_cmd"] then
            docroot_default = false
            flush_cmd_default = conf["default"]["vhost"]["flush_cmd"]
          elsif conf["default"]["vhost"]["docroot"] then
            docroot_default = conf["default"]["vhost"]["docroot"]
            flush_cmd_default = false
          end
        end
      end

      if conf["stages"] && conf["stages"]["#{stage_name}"] then
        conf_stage = conf["stages"]["#{stage_name}"]
        if conf_stage["vhost"] then
          if conf_stage["vhost"]["flush_cmd"] then
            docroot_default = false
            flush_cmd_default = conf_stage["vhost"]["flush_cmd"]
          elsif conf_stage["vhost"]["docroot"] then
            docroot_default = conf_stage["vhost"]["docroot"]
            flush_cmd_default = false
          end
        end
      end

      servers = find_servers_for_task(current_task)
      servers.each do |server|
        system "echo #{server}"
        if conf["servers"] && conf["servers"]["#{server}"] then
          server_conf = conf["servers"]["#{server}"]
          docroot = false
          flush_cmd = false

          if server_conf["vhost"] then
            if server_conf["vhost"]["flush_cmd"] then
              docroot = false
              flush_cmd = server_conf["vhost"]["flush_cmd"]
            elsif server_conf["vhost"]["docroot"] then
              docroot = server_conf["vhost"]["docroot"]
              flush_cmd = false
            end
          end


        end
        if docroot || flush_cmd then
          if flush_cmd then
            run flush_cmd, :hosts => server
          elsif docroot && docroot.start_with?("/aem/web") then
            run "rm -rf #{docroot}/*", :hosts => server
          end
        elsif docroot_default || flush_cmd_default then
          if flush_cmd_default then
            run flush_cmd_default, :hosts => server
          elsif docroot_default && docroot_default.start_with?("/aem/web") then
            run "rm -rf #{docroot_default}/*", :hosts => server
          end
        end
      end
    end

  end
end