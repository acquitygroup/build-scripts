require "fileutils"

namespace :output do

	set :_target, "#{Dir.pwd}/target"
	set :_output, "#{_target}/out"

	# override :output_type to "zip" to create a zip file instead of a tar.gz file
	set :output_type, "tar.gz"
	# override :final_name if different from :application
	set :final_name, "#{application}"

	task :clean do
		FileUtils.rmtree(dir)
	end

	task :build do
		type = "#{output_type}"
		if File.exist?(finalpath) then
			File.delete(finalpath)
		end

		if type == "zip" then
			system "cd '#{outdir}'; zip -r -x='.svn' '#{finalpath}' ." 
		elsif type == "tar.gz"
			system "tar -czf '#{finalpath}' --exclude='.svn' -C '#{outdir}' ."
		end
	end
		
	task :upload_artifact do
		upload finalpath, remotepath, :via => :scp
	end

	task :create_release_path do
		run "mkdir -p '#{release_path}'"
	end

	task :extract do
		type = "#{output_type}"

		if type == "zip" then
			run "unzip -o '#{remotepath}' -d '#{release_path}/'"
		elsif
			run "tar -xzf '#{remotepath}' -C '#{release_path}'"
		end
	end

	def dir() 
		unless File.directory?("#{_target}")
			FileUtils.makedirs("#{_target}")
		end
		"#{_target}"
	end

	def outdir()
		unless File.directory?("#{_output}")
			FileUtils.makedirs("#{_output}")
		end
		"#{_output}"
	end

	def output_filename()
		"#{final_name}.#{output_type}"
	end

	def remotepath()
		"#{deploy_to}/#{output_filename}"
	end

	def finalpath()
    puts "#{dir}/#{output_filename}"
		"#{dir}/#{output_filename}"
	end

end
