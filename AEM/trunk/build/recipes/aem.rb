require "fileutils"

namespace :aem do
  set :service_command, "sudo -nb /sbin/service"

  set :cq_runmode, ""
  set :cq_jvm_opts, "-server -Xmx2g -XX:MaxPermSize=512M -Djava.awt.headless=true -XX:+HeapDumpOnOutOfMemoryError"

  set :local_jar_path, File.dirname(__FILE__) + "/aem/crx-quickstart-5.6.1.jar"
  set :local_lic_path, File.dirname(__FILE__) + "/aem/license.properties"

  set :httpsig_api, File.dirname(__FILE__) + "/aem/net.adamcin.httpsig.osgi-1.0.4.jar"
  set :httpsig_sling, File.dirname(__FILE__) + "/aem/net.adamcin.sling.auth.httpsig-0.8.2.jar"

  set :aem_bin_dir, "/aem/bin"
  set :aem_apps_dir, "/aem/apps"

  task :install, :roles => [:author, :publish] do
    aem.upload_bin
    aem.install_author
    aem.install_publish
  end

  task :install_author, :roles => [:author], :on_no_matching_servers => :continue do
    dist_aem("#{aem_apps_dir}/author")
  end

  task :install_publish, :roles => [:publish], :on_no_matching_servers => :continue do
    dist_aem("#{aem_apps_dir}/publish")
  end

  task :upload_bin, :roles => [:author, :publish], :on_no_matching_servers => :continue do
    platform.exists_or_run(aem_bin_dir, "aem", "mkdir -p #{aem_bin_dir}")
    if !platform.remote_file_exists?("#{jar_bin_path}", "aem") then
      upload "#{local_jar_path}", "#{jar_bin_path}", :via => :scp
    end
  end

  def self.jar_bin_path
    "#{aem_bin_dir}/" + File.basename("#{local_jar_path}")
  end

  def dist_aem(app_dir)
    platform.exists_or_run(app_dir, "aem", "umask u=rwx,go=rx; mkdir -p #{app_dir}")
    upload "#{local_lic_path}", "#{app_dir}/license.properties", :via => :scp
    platform.exists_or_run(app_dir + "/crx-quickstart", "aem", "/bin/sh -c 'cd #{app_dir}; java -jar #{jar_bin_path} -unpack'")
  end

  task :configure, :roles => [:author, :publish] do
    configure_author
    configure_publish
  end

  task :configure_author, :roles => [:author], :on_no_matching_servers => :continue do
    run_modes = "#{cq_runmode}".strip.empty? ? "author" : "author,#{cq_runmode}"
    configure_start_script("#{aem_apps_dir}/author", {"CQ_PORT" => 4502, "CQ_RUNMODE" => run_modes, "CQ_JVM_OPTS" => "#{cq_jvm_opts}"})
  end

  task :configure_publish, :roles => [:publish], :on_no_matching_servers => :continue do
    run_modes = "#{cq_runmode}".strip.empty? ? "publish" : "publish,#{cq_runmode}"
    configure_start_script("#{aem_apps_dir}/publish", {"CQ_PORT" => 4503, "CQ_RUNMODE" => run_modes, "CQ_JVM_OPTS" => "#{cq_jvm_opts}"})
  end

  def configure_start_script(app_dir, params={})
    params.each do |param, value|
      platform.run_as_aem "sed -i '/.*$.*#{param}.*/! s/^\\(\\s*#{param}=\\).*$/\\1\"#{value}\"/' #{app_dir}/crx-quickstart/bin/start"
    end
  end

  task :install_init, :roles => [:author, :publish] do
    install_init_author
    install_init_publish
  end

  task :install_init_author, :roles => [:author], :on_no_matching_servers => :continue do
    init_script_name = "aem-author"
    cq_root = "#{aem_apps_dir}/author"
    install_init_script(init_script_name, cq_root)
  end

  task :install_init_publish, :roles => [:publish], :on_no_matching_servers => :continue do
    init_script_name = "aem-publish"
    cq_root = "#{aem_apps_dir}/publish"
    install_init_script(init_script_name, cq_root)
  end

  def install_init_script(init_script_name, cq_root)
    unless File.exists?("target/#{init_script_name}")
      template = File.read(File.dirname(__FILE__) + "/aem/aem-init.erb")
      buffer = ERB.new(template).result(binding)
      FileUtils.makedirs("target")
      File.open("target/#{init_script_name}", 'w') { |f| f.write(buffer) }
    end
    upload "target/#{init_script_name}", "/tmp/#{init_script_name}"
    run "sudo -n cp -f /tmp/#{init_script_name} /etc/rc.d/init.d/#{init_script_name}"
    run "sudo -n chmod 0755 /etc/rc.d/init.d/#{init_script_name}"
    run "sudo -n chkconfig --add #{init_script_name}"
  end

  task :start, :roles => [:author, :publish] do
    start_author
    start_publish
  end

  task :start_author, :roles => :author do
    run "#{service_command} aem-author start"
  end

  task :start_publish, :roles => :publish do
    run "#{service_command} aem-publish start"
  end

  task :stop, :roles => [:author, :publish] do
    stop_author
    stop_publish
  end

  task :stop_author, :roles => :author do
    run "#{service_command} aem-author stop"
  end

  task :stop_publish, :roles => :publish do
    run "#{service_command} aem-publish stop"
  end

  task :restart, :roles => [:author, :publish] do
    restart_author
    restart_publish
  end

  task :restart_author, :roles => :author do
    run "#{service_command} aem-author restart"
  end

  task :restart_publish, :roles => :publish do
    run "#{service_command} aem-publish restart"
  end

  task :httpsig, :roles => [:author, :publish],
       :on_no_matching_servers => :continue do
    httpsig_author
    httpsig_publish
  end

  task :httpsig_author, :roles => [:author],
       :on_no_matching_servers => :continue do
    upload_httpsig("#{aem_apps_dir}/author")
  end

  task :httpsig_publish, :roles => [:publish],
       :on_no_matching_servers => :continue do
    upload_httpsig("#{aem_apps_dir}/publish")
  end

  def upload_httpsig(cq_root)
    platform.exists_or_run("#{cq_root}/crx-quickstart/install", "aem", "mkdir #{cq_root}/crx-quickstart/install")
    upload "#{httpsig_api}", "#{cq_root}/crx-quickstart/install", :via => :scp
    upload "#{httpsig_sling}", "#{cq_root}/crx-quickstart/install", :via => :scp
  end
end