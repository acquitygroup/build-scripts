namespace :httpd do

  set :author_root, "/aem/web/author"
  set :publish_root, "/aem/web/publish"
  set :httpd_modules, "/usr/lib64/httpd/modules"

  task :install, :roles => [:www_author, :www_publish],
       :on_no_matching_servers => :continue do
    run "sudo -n yum -y install httpd mod_ssl"
    if platform.remote_file_exists?("/tmp/dispatcher", "") then
      run "rm -rf /tmp/dispatcher"
    end
    run "mkdir -p /tmp/dispatcher"
    upload "#{local_disp_file}", "/tmp/dispatcher.tar.gz"
    run "tar -zxvf /tmp/dispatcher.tar.gz -C /tmp/dispatcher"
    run "sudo -n find /tmp/dispatcher -name '*.so' -exec cp '{}' /etc/httpd/modules/mod_dispatcher.so \\; -quit"
    run "sudo chmod a+rx /etc/httpd/modules/mod_dispatcher.so"
    run "rm /tmp/dispatcher.tar.gz"
    run "rm -rf /tmp/dispatcher"
  end

  task :instance, :roles => [:www_author, :www_publish],
       :on_no_matching_servers => :continue do

    instance_author
    instance_publish
  end

  task :instance_author, :roles => [:www_author],
       :on_no_matching_servers => :continue do

    establish_instance("#{author_root}")
  end

  task :instance_publish, :roles => [:www_publish],
       :on_no_matching_servers => :continue do
    establish_instance("#{publish_root}")
  end

  def establish_instance(web_root)
    platform.run_as_aem("mkdir -p #{web_root}")
    platform.run_as_aem("mkdir -p #{web_root}/logs")
    platform.run_as_aem("mkdir -p #{web_root}/run")
    platform.run_as_aem("mkdir -p #{web_root}/conf")
    platform.run_as_aem("mkdir -p #{web_root}/docroot")
    platform.run_as_aem("ln -s #{httpd_modules} #{web_root}/modules")
  end

  task :install_init, :roles => [:www_author, :www_publish],
       :on_no_matching_servers => :continue do
    install_init_author
    install_init_publish
  end

  task :install_init_author, :roles => [:www_author],
       :on_no_matching_servers => :continue do
    install_init_script("aem-web-author", "#{author_root}")
  end

  task :install_init_publish, :roles => [:www_publish],
       :on_no_matching_servers => :continue do
    install_init_script("aem-web-publish", "#{publish_root}")
  end

  def install_init_script(init_script_name, httpd_root)
    unless File.exists?("target/#{init_script_name}")
      template = File.read(File.dirname(__FILE__) + "/aem/aem-web-init.erb")
      buffer = ERB.new(template).result(binding)
      FileUtils.makedirs("target")
      File.open("target/#{init_script_name}", 'w') { |f| f.write(buffer) }
    end
    upload "target/#{init_script_name}", "/tmp/#{init_script_name}"
    run "sudo -n cp -f /tmp/#{init_script_name} /etc/rc.d/init.d/#{init_script_name}"
    run "sudo -n chmod 0755 /etc/rc.d/init.d/#{init_script_name}"
    run "sudo -n chkconfig --add #{init_script_name}"
  end
end