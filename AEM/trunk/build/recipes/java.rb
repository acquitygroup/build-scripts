require "fileutils"
# good instructions here: http://ivan-site.com/2012/05/download-oracle-java-jre-jdk-using-a-script/
namespace :java do
  expect_java_version = "1.7.0_51"
  download_suffix = "7u51-b13/jdk-7u51-linux-x64.rpm"
  jdk_rpm_filename = File.basename(download_suffix)

  task :version do
    puts actual_java_version
  end

  task :install do
    unless expect_java_version == actual_java_version()
      run "curl -L -H 'Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F' -o /tmp/#{jdk_rpm_filename} http://download.oracle.com/otn-pub/java/jdk/#{download_suffix}"
      run "sudo -n rpm -Uvh /tmp/#{jdk_rpm_filename}"
      run "sudo -n alternatives --install /usr/bin/java java /usr/java/jdk#{expect_java_version}/jre/bin/java 20000"
      run "sudo -n alternatives --install /usr/bin/javaws javaws /usr/java/jdk#{expect_java_version}/jre/bin/javawc 20000"
      run "sudo -n alternatives --install /usr/bin/javac javac /usr/java/jdk#{expect_java_version}/jre/bin/javac 20000"
      run "sudo -n alternatives --install /usr/bin/jar jar /usr/java/jdk#{expect_java_version}/jre/bin/jar 20000"
    end
  end

  def actual_java_version
    version = capture "java -version 2>&1 | grep \"java version\" | awk '{print $3}' | tr -d \\\""
    version.strip
  end
end
