namespace :platform do

  set :useradd_command, "sudo -n /usr/sbin/useradd"
  set :usermod_command, "sudo -n /usr/sbin/usermod"
  set :groupadd_command, "sudo -n /usr/sbin/groupadd"
  set :service_command, "sudo -nb /sbin/service"

  set :aem_keys_file, "aem_keys"

  set :local_disp_file, File.dirname(__FILE__) + "/platform/dispatcher-apache2.2-linux-x86-64-4.1.5.tar.gz"


  task :echo do
    run "echo hey"
  end

  task :aem_space do
    ensure_user_and_groups("aem")
    unset_password_expiry("aem")
    exists_or_run("/aem", "root", "mkdir /aem")
    run_if_exists("/aem", "root", "chown aem:aem /aem")
  end

  task :aem_keys do
    local_auth_keys = File.dirname(__FILE__) + "/platform/#{aem_keys_file}"
    upload local_auth_keys, "/tmp/aem_keys"
    run "chmod a+r /tmp/aem_keys"
    exists_or_run("~aem/.ssh", "aem", "ssh-keygen -t rsa -b 2048 -N '' -f ~aem/.ssh/id_rsa", false)
    run_as_aem "cp /tmp/aem_keys ~aem/.ssh/authorized_keys"
    run_as_aem "chmod 600 ~aem/.ssh/authorized_keys"
    run "rm /tmp/aem_keys"
  end

  task :aem_sudoers do
    exists_or_run("/etc/sudoers.d/aem", "root", "chmod 0640 /etc/sudoers.d/aem", true)
    run "echo '%aem\tALL=(root) NOPASSWD: /sbin/service aem-*' > /tmp/aem.sudoers"
    run "sudo -n bash -c 'cp /tmp/aem.sudoers /etc/sudoers.d/aem; chmod 0440 /etc/sudoers.d/aem'"
    run "rm /tmp/aem.sudoers"
    #run "sudo -n chmod 0440 /etc/sudoers.d/aem"
  end


  def run_as_aem(command)
    if "#{user}" == "aem" then
      run command
    else
      run "sudo -nu aem #{command}"
    end
  end

  def run_if_exists(full_path, run_as = "", command = "echo true", reverse = false, options = {})
    bang = reverse ? "!" : ""
    if run_as.empty? || "#{user}" == run_as then
      run("[ #{bang} -e #{full_path} ] && #{command}", options)
    else
      run("sudo -nu #{run_as} [ #{bang} -e #{full_path} ] && sudo -nu #{run_as} #{command}", options)
    end
  end

  def exists_or_run(full_path, run_as = "", command = "echo true", reverse = false, options = {})
    bang = reverse ? "!" : ""
    if run_as.empty? || "#{user}" == run_as then
      run("[ #{bang} -e #{full_path} ] || #{command}", options)
    else
      run("sudo -nu #{run_as} [ #{bang} -e #{full_path} ] || sudo -nu #{run_as} #{command}", options)
    end
  end


  def remote_file_exists?(full_path, run_as = "", host = "")
    if run_as.empty? || "#{user}" == run_as then
      'true' == capture("if [ -e #{full_path} ]; then echo 'true'; fi", :hosts => host).strip
    else
      #'true' == capture("if `sudo -nu #{run_as} [ -e #{full_path} ]`; then echo 'true'; fi").strip
      'true' == capture("sudo -nu #{run_as} bash -c 'if [ -e #{full_path} ]; then echo true; fi'", :hosts => host).strip
    end
  end

  def ensure_user_and_groups(user = "", groups = [])
    unless user.empty?
      run "groups #{user}; if [ \"0\" != \"$?\" ]; then #{useradd_command} #{user}; fi"
      groups.each { |group|
        run "#{groupadd_command} #{group}; if [ \"0\" != \"$?\" ]; then echo failed to create group; fi"
        run "if [ `groups #{user} | cut -d: -f2 | grep #{group} | wc -l` ]; then #{usermod_command} -a -G #{group} #{user}; fi"
      }
    end
  end

  def unset_password_expiry(username)
    run "sudo -n chage -I -1 -m 0 -M 99999 -E -1 #{username}"
  end

end