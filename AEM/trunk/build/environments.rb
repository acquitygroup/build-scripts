task :dev do
  set :cq_runmode, "dev"
  server "162.242.135.247", :author, :publish, :www_author, :www_publish
end

task :qa do
  set :cq_runmode, "qa"
  server "162.242.142.12", :author, :publish
  server "162.242.135.246", :www_author, :www_publish
end
