task :forddev do
	set :deploy_to, "/etc/httpd/dev/ford/author/conf"
	set :user, "ford"
  server "10.12.40.19", :www
end

desc 'set dev env settings'
task :dev_author do
  set :deploy_to, "/etc/httpd/dev/ford/author"
  server "10.12.40.19", :www, :www_author

  #DocumentRoot /var/www/final/fmc/author
end

task :dev_publish do
  server "10.12.40.19", :www, :www_author
end
