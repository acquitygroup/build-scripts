require "fileutils"
namespace :httpd do

  namespace :build do

		inc_httpd = File.dirname(__FILE__) 

    task :build do
			copy_static
      httpd.build.install_revisiontxt
    end

		task :copy_static do
			inc_static_dir = inc_httpd + "/static"
			inc_children = Dir.foreach(inc_static_dir).reject {|filename| filename.start_with? "."}
			FileUtils.cp_r inc_children.map {|fn| "#{inc_static_dir}/#{fn}"}, output.outdir

			static_dir = "src/static"
			children = Dir.foreach(static_dir).reject {|filename| filename.start_with? "."}
			FileUtils.cp_r children.map {|fn| "#{static_dir}/#{fn}"}, output.outdir
		end

    task :install_revisiontxt do
			revision_file = "#{output.outdir}/revision.txt"
			system "svn info > '#{revision_file}'"
    end

  end
end
