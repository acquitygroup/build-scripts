namespace :httpd do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  namespace :configure do

    task :start, roles => :web do
      run "service httpd restart"
    end

    task :install_httpd_conf, roles => :web, :except => { :no_release => true } do
      template = File.read("#{apache_httpd_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf/httpd.conf"
    end

    task :install_apache_conf, roles => :web, :except => { :no_release => true } do
      template = File.read("#{apache_slave_vhost_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf.d/#{site_name}.conf"
      template = File.read("#{apache_master_vhost_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf.d/master.#{site_name}.conf"
    end

    task :install_default_site, roles => :web, :except => { :no_release => true } do
      template = File.read("#{default_site_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/var/www/html/index.html"
    end

  end
end