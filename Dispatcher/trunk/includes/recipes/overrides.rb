default_run_options[:pty] = false
default_run_options[:shell] = false


namespace :clean do
  task :default do
    output.clean
  end
end

namespace :build do
  task :default do
    deploy.build
  end
end

namespace :deploy do

  task :default do
    update
    transaction do
      restart_or_rollback
    end
  end

  task :build do
    httpd.build.build
    output.build
  end

  task :cold do
    setup
    start
  end


  task :restart, :except => { :no_release => true } do
    httpd.deploy.restart
  end

  task :restart_or_rollback, :except => { :no_release => true } do
    on_rollback { 
      deploy.rollback.code
      restart
    }

    restart
  end

  task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end

    output.create_release_path
    output.upload_artifact
    output.extract
    httpd.deploy.after_extract
    finalize_update
  end

  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end

  task :finalize_update do
    httpd.deploy.stop
    httpd.deploy.verify
    cleanup
  end

  task :setup do
    set :user, "root"
    # TODO: provide setup logic
  end

  task :start do
    httpd.deploy.start
  end

end

after 'deploy:restart', "deploy:after_restart"
