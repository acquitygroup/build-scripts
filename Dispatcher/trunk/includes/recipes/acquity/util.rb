namespace :acquity do


  namespace :util do

    task :start_vpn do
      print "Checking for running VPN to #{vpn_datacenter}.\n"
      vpncheck = `ps -ef | grep vpnc | grep root`
      if vpncheck.include? "vpnc #{vpn_datacenter}" then
        print "VPN to #{vpn_datacenter} Running, skipping."
      else
        print "Starting VPN to #{vpn_datacenter}.\n"
        system "sudo /usr/local/sbin/vpnc #{vpn_datacenter}"
        if $?.exitstatus != 0 then
          raise "VPN Start Failure!"
        end
      end
    end

    task :stop_vpn do
      print "Stopping VPN.\n"
      system "sudo /usr/local/sbin/vpnc-disconnect"
      if $?.exitstatus != 0 then
        raise "VPN Stop Failure!"
      end
    end

  end
end