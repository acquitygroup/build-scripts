load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/httpd/configure'
load File.dirname(__FILE__) + '/httpd/build'
load File.dirname(__FILE__) + '/httpd/deploy'
load File.dirname(__FILE__) + '/httpd/util'

namespace :httpd do
  task :setup, roles => [:www, :www_author], :except => { :no_release => true } do

  end

  task :symlink, roles => [:www, :www_author], :except => { :no_release => true } do

  end

  task :update_code, roles => [:www, :www_author], :except => { :no_release => true } do

  end


  task :install_shared_symlinks, roles => [:www, :www_author], :except => { :no_release => true } do

  end

end