load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/endeca/configure'
load File.dirname(__FILE__) + '/endeca/build'
load File.dirname(__FILE__) + '/endeca/deploy'
load File.dirname(__FILE__) + '/endeca/util'
set :tomcat_version, "7.0.27"

namespace :endeca do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 