namespace :endeca do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  namespace :deploy do
    
    task :create_release_path, :roles => :app do
      run "mkdir -p #{release_path}"
    end
    
    task :copy_endeca, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../Packages/Endeca.zip", "#{deploy_to}/Endeca.zip"      
    end

    task :deploy_endeca, :roles => :app do
      run "unzip -o '#{deploy_to}/Endeca.zip' -d #{release_path}/"
    end

    #Deprecated as current symlinks do not work with endeca
    task :setup_endeca_old, :roles => :app do
      run "ln -s #{deploy_to}/persist #{release_path}/persist"
      run "ln -s #{deploy_to}/data #{release_path}/data"
      run "ln -s #{deploy_to}/logs #{release_path}/logs"
      run "ln -s #{deploy_to}/reports #{release_path}/reports"      
      #Force executable for control scripts
      run "cd #{release_path}/config; find . -name *.sh -exec chmod +rx {} \\;"
      run "cd #{release_path}/config; find . -name *.pl -exec chmod +rx {} \\;"
      run "cd #{release_path}/control; chmod +x *.sh"
      # If development environment.proeprties checked in, nuke.
      run "cd #{release_path}/config/script; rm environment.properties"    
      servers = find_servers_for_task(current_task)
      servers.each_with_index do |s,index|
        puts "Index is #{index}"
        run "sed -i 's/HOST-IP/$CAPISTRANO:HOST$/g' #{release_path}/config/script/environment_#{stage_name}.properties", :hosts => s       
      end
      run "cd #{release_path}/config/script; ln -s environment_#{stage_name}.properties environment.properties"
      run "rm -f #{deploy_to}/revision.txt; cp #{release_path}/revision.txt #{deploy_to}/revision.txt"
    end
    
     task :setup_endeca, :roles => :app do      
      #Force executable for control scripts
      run "cd #{release_path}/config; find . -name *.sh -exec chmod +rx {} \\;"
      run "cd #{release_path}/config; find . -name *.pl -exec chmod +rx {} \\;"
      run "cd #{release_path}/control; chmod +x *.sh"
      # If development environment.proeprties checked in, nuke.
      run "cd #{release_path}/config/script; rm environment.properties"    
      servers = find_servers_for_task(current_task)
      servers.each_with_index do |s,index|
        puts "Index is #{index}"
        run "sed -i 's/HOST-IP/$CAPISTRANO:HOST$/g' #{release_path}/config/script/environment_#{stage_name}.properties", :hosts => s       
      end
      run "cd #{release_path}/config/script; ln -s environment_#{stage_name}.properties environment.properties"
      run "rm -f #{deploy_to}/revision.txt; cp #{release_path}/revision.txt #{deploy_to}/revision.txt"
      #rsync any directories to main folder.
      run "find #{release_path} -mindepth 1 -maxdepth 1 -type d -exec rsync -rav {} #{deploy_to} \\;"
    end
 
    task :verify_endeca, :roles => :app do       
      run "cat #{release_path}/revision.txt"
    end
    
    task :release_lock, :roles => :app do
      run "cd #{deploy_to}/control; ./runcommand.sh LockManager releaseLock update_lock"
    end
   
    task :baseline, :roles => :app do
      #changed from #{current_path} if symlinks worked
      run "cd #{deploy_to}/control; ./baseline_update.sh"
    end
  
    task :promote, :roles => :app do
      #changed from #{current_path} if symlinks worked
      run "cd #{deploy_to}/control; ./promote_content.sh"
    end
  
    task :rollback, :roles => :app do
      run "find #{previous_release} -mindepth 1 -maxdepth 1 -type d -exec rsync -rav {} #{deploy_to} \\;"
    end
    
    task :stop_endeca, :roles => :app, :on_error => :continue do
      #run "cd '#{deploy_to}/bin'; #{java_home_cmd}; ./tcserver.sh #{stage_name} stop"
      #run "#{sudo} service httpd stop"
    end

    task :restart_endeca, :roles => :app do
      
      #Cut readd when sudoers works right
      #run "#{sudo} service httpd restart"
    end

        
  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
