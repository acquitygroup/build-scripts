namespace :endeca do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do

  
    
    task :build do
      endeca.build.install_revisiontxt          
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "mkdir -p '#{dirname}/../../../../Packages'"      
      run_locally "rm -rf '#{dirname}/../../../../Packages/*'"   
      #run_locally "cd '#{dirname}/../../../../'; rsync -rav --delete build/includes/app/endeca-#{endeca_version}/ app/endeca/"
      run_locally "cd '#{dirname}/../../../../app'; rm -f '#{dirname}/../../../../Packages/Endeca.zip'; zip -r -x='.svn' '#{dirname}/../../../../Packages/Endeca.zip' *"
    end

    task :install_revisiontxt do
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "rm -f '#{dirname}/../../../../app/revision.txt'"
      run_locally "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../app'/revision.txt; true"
      if ENV['BUILD_TAG'] then
        build_tag = ENV['BUILD_TAG']
      else 
        build_tag = "No build tag parameter found"
      end
      File.open("#{dirname}/../../../../app/revision.txt", 'a') { |f2|  f2.puts "Jenkins Build Tag: #{build_tag}" }
      
    end
        
  end
end