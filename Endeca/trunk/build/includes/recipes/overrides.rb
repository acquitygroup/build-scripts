default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    update
    baseline
    promote
    #restart
  end

  task :build do
    endeca.build.build
  end

  task :cold do
    setup
    start
  end

  task :baseline, :except => { :no_release => true } do
   if fetch(:skip_baseline, false) == false then
      endeca.deploy.baseline
    end  
  end
  
  task :promote, :except => { :no_release => true } do
    endeca.deploy.promote
  end

  task :restart, :except => { :no_release => true } do
    endeca.deploy.restart_endeca
  end


   task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    endeca.deploy.create_release_path
    endeca.deploy.copy_endeca 
    finalize_update
  end
  
  task :flip do
     if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    endeca.deploy.flip_status
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :verify_flip do
     if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    endeca.deploy.verify_down
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end

 
  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update do
    endeca.deploy.deploy_endeca
    endeca.deploy.setup_endeca
    endeca.deploy.verify_endeca
    cleanup      
   end

  task :setup do
    set :user, "root"
    #endeca.configure.fix_sudoers_for_cap
    #endeca.configure.install_audit_tools
    #endeca.configure.set_hostname
    #endeca.configure.install_svn_directories
    #endeca.configure.install_apache
    #endeca.configure.install_endeca
    #endeca.configure.install_deploy_keys
    #endeca.configure.install_viewvc
    #endeca.configure.sync_svn_conf
    #endeca.configure.install_fuse
    #endeca.configure.mount_fuse
    #endeca.configure.start_http_server
    #endeca.configure.install_cron_scripts
    #endeca.configure.install_global_hooks
    #endeca.configure.install_master_symlinks
    #endeca.configure.install_slave_symlinks
    #endeca.configure.start_cron
  end

  task :start do
    endeca.configure.start_http_server
  end


end

before 'deploy:rollback', "endeca:deploy:rollback"
after 'deploy:restart', "deploy:after_restart"