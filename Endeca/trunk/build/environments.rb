desc "deploy to master environment"
task :master do
  set :stage_name, "master"

end

desc "deploy to development environment"
task :dev do
  set :stage_name, "dev"  
  set :user, "endeca"
  set :deploy_to, "/usr/local/endeca/apps/IR"
  role :app, "10.18.30.103"  
end

desc "deploy to stg environment"
task :stg do
  set :stage_name, "stg"  
  set :user, "endeca"
  set :deploy_to, "/usr/local/endeca/apps/IR"
  role :app, "10.18.31.103"  
end

desc "UAT Environment"
task :UAT do
  set :stage_name, "uat"
  set :site_name, "uat.acq.acq"
  #role :app, "172.26.48.58"
  #role :web, "172.26.48.58"
end

desc "deploy to prod environment"
task :prod do
  set :stage_name, "prod"

end