desc "deploy to development environment"
task :local do
  set :stage_name, "local"
  set :site_name, "cq5.local"
  set :user, "root"
  set :deploy_to, "/usr/applications/ag-commerce-cq5-local"
  role :author, "author.cq5.local"
  role :publish, "publish.cq5.local"
  role :web, ""
end



desc "deploy to development environment"
task :dev do
  set :stage_name, "dev"
  set :site_name, "cq5.local"
  set :username, "root"
  set :deploy_to, "/usr/applications/ag-commerce-cq5-dev"
  role :app, "10.12.40.17"
  role :web, "10.12.40.17"
end



desc "deploy to qa environment"
task :qa do
  set :stage_name, "qa"
  set :site_name, "qa.acq.acq"
  set :username, "acq"
  set :deploy_to, "/usr/applications/ag-commerce-acq-qa"
  role :app, "10.50.40.60"
  role :app, "10.50.40.61"
  role :web, "10.50.40.60"
  role :web, "10.50.40.61"
end

desc "UAT Environment"
task :UAT do
  set :stage_name, "uat"
  set :site_name, "uat.acq.acq"
  #role :app, "172.26.48.58"
  #role :web, "172.26.48.58"
end

desc "deploy to prod environment"
task :prod do
  set :stage_name, "prod"

end
