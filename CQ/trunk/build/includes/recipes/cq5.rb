load File.dirname(__FILE__) + '/cq5/configure'
set :tomcat_version, "7.0.32"
set :cq5_version, "5.4.0"
set :java_jdk_version, "6u31"
set :vpnc_version, "0.5.3"

namespace :cq5 do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end


end 
