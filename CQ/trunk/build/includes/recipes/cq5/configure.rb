namespace :cq5 do

  set :server_script_template, File.dirname(__FILE__) + "/server_script.erb"
  set :tomcat_server_xml_template, File.dirname(__FILE__) + "/tomcat_server_xml.erb"
  set :apache_httpd_conf_template, File.dirname(__FILE__) + "/apache_httpd_conf.erb"
  set :apache_vhost_conf_template, File.dirname(__FILE__) + "/apache_vhost_conf.erb"
  set :default_site_template, File.dirname(__FILE__) + "/default_site.erb"
  set :createdb_template, File.dirname(__FILE__) + "/createdb.erb"
  set :my_cnf_template, File.dirname(__FILE__) + "/my_cnf.erb"
  set :cq5_filesystem_mysql_repo_template, File.dirname(__FILE__) + "/cq5_filesystem_mysql_repo.erb"
  set :cq5_system_properties_template, File.dirname(__FILE__) + "/cq5_system_properties.erb"
  set :cq5_config_xml_template, File.dirname(__FILE__) + "/cq5_config_xml.erb"
  set :cq5_security_xml_template, File.dirname(__FILE__) + "/cq5_security_xml.erb"
  set :cq5_license_template, File.dirname(__FILE__) + "/cq5_license.erb"
  
  namespace :configure do

    task :fix_sudoers_for_cap do
      run "#{sudo} /bin/bash -c \"sed -i 's/ requiretty/!requiretty/' /etc/sudoers; chmod 0440 /etc/sudoers\""
      run "#{sudo} /bin/bash -c \"sed -i 's/ !visiblepw/visiblepw/' /etc/sudoers; chmod 0440 /etc/sudoers\""
    end

    task :set_hostname do
      if site_name == "cq5.local"
        run "hostname #{site_name}"
      end
    end

    task :install_cq5_directories, :roles => :app, :on_error => :continue  do
      run "#{sudo} mkdir -p /releng"
      run "#{sudo} groupadd cq5"
      run "#{sudo} useradd -g cq5 -G cq5 -d /usr/applications/cq5 cq5"
  
      run "#{sudo} chown -R cq5:cq5 /usr/applications/cq5"
      run "#{sudo} chmod 700 /usr/applications/cq5/.ssh"
      #Chcons are for Centos 6
      run "#{sudo} chcon unconfined_u:object_r:user_home_dir_t:s0 /usr/applications/cq5"
      run "#{sudo} chcon unconfined_u:object_r:ssh_home_t:s0 /usr/applications/cq5/.ssh"
     
    end

    task :install_deploy_keys, :roles => :app do
     
    end

    task :install_java, :roles => :app do
      run "#{sudo} yum -y remove java"
      run "#{sudo} rm -rf /tmp/*.rpm"
      run "#{sudo} smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"Java\";get jdk-#{java_jdk_version}-linux-x64-rpm.bin /tmp/jdk-#{java_jdk_version}-linux-x64.rpm.bin;exit' "
      run "#{sudo}  /bin/bash -c \"cd /tmp; unzip -o /tmp/jdk-#{java_jdk_version}-linux-x64.rpm.bin; true\""
      run "#{sudo} rpm -Uvf /tmp/jdk*.rpm; true"
    end
    
    task :install_mysql, :roles => :app do
      run "#{sudo} yum -y install mysql mysql-server"
      run "#{sudo} chkconfig --level 5 mysqld on" 
      run "#{sudo} mkdir -p /drive1/mysql"
      run "#{sudo} rm -rf /var/lib/mysql"
      run "#{sudo} ln -s  /drive1/mysql /var/lib/mysql"
      run "#{sudo} chcon -R system_u:object_r:mysqld_db_t:s0 /drive1/mysql"
      run "#{sudo} semanage fcontext -a -t mysqld_db_t \"/drive1/mysql(/.*)?\""
      run "#{sudo} restorecon -Rv /drive1/mysql"
      #run "#{sudo} chcon -R system_u:object_r:mysqld_db_t:s0 /var/lib/mysql"
      cq5.configure.install_my_cnf
      stages.each { |stage|
        set :stage_name, stage
        run "#{sudo} mkdir -p /usr/applications/cq5/misc/mysql"
        cq5.configure.install_createdb
      }
          
    end
    
    task :configure_mysql, :roles => :app do
    
      run "#{sudo} mysql -fv < /usr/applications/cq5/misc/mysql/createdb.sql"
      
    end

    task :install_mysql_connector, :roles => :app do
      run "#{sudo} wget -q -O /tmp/mysql-connector-java-5.1.21.zip http://www.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.21.zip/from/http://cdn.mysql.com/"
      run "#{sudo} /bin/bash -c \"cd /tmp; unzip -o mysql-connector-java-5.1.21.zip\""   
      run "#{sudo} cp -u /tmp/mysql-connector-java-5.1.21/mysql-connector-java-5.1.21-bin.jar /usr/local/apache-tomcat-#{tomcat_version}/lib"
    end
    
    task :install_tomcat, :roles => :app  do
      run "#{sudo} wget -q -O /usr/local/apache-tomcat-#{tomcat_version}.tar.gz http://archive.apache.org/dist/tomcat/tomcat-#{tomcat_version[0,1]}/v#{tomcat_version}/bin/apache-tomcat-#{tomcat_version}.tar.gz"
      run "#{sudo} /bin/bash -c \"cd /usr/local; tar -zxf apache-tomcat-#{tomcat_version}.tar.gz\""
      run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/tomcat;\""
      run "#{sudo} cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /usr/applications/cq5/tomcat"
      run "#{sudo} /bin/bash -c  \"mkdir -p /usr/applications/cq5/tomcat/logs;\""
      run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/tomcat/temp;\""
      run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/tomcat/webapps;\""
      run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/tomcat/work;\""
      run "#{sudo} chown -R cq5:cq5 /usr/applications/cq5"
      run "#{sudo} chmod 775 /usr/applications/cq5"
      stages.each { |stage|
        set :stage_name, stage
        cq5.configure.install_server_script
        cq5.configure.install_server_xml  
      }
    end
    
  task :install_cq5, :roles => :app do
     
      run "#{sudo} smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"Day/5/Software\";get cq-quickstart-#{cq5_version}.war /tmp/cq-quickstart-#{cq5_version}.war;exit' "
      #run "#{sudo} /bin/bash -c \"cd /tmp; tar -zxf atlassian-cq5-#{cq5_version}.tar.gz\""
      
      #run "#{sudo} /bin/bash -c \"cd /tmp/atlassian-cq5-#{cq5_version}-war;chmod +x build.sh;./build.sh\""
      
      run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/;\""
      #run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/tomcat/webapps/ROOT/;\""
      #run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/etc/repo/filesystem-mysql; mkdir -p /usr/applications/cq5/internal/cq5/etc/repo/filesystem-mysql; mkdir -p /usr/applications/cq5/external/cq5/etc/repo/filesystem-mysql\""
      
      #run "#{sudo} mkdir -p /usr/applications/cq5/plugins; mkdir -p /usr/applications/cq5/dev/cq5/plugins; mkdir -p /usr/applications/cq5/qa/cq5/plugins; mkdir -p /usr/applications/cq5/prod/cq5/plugins"
      
      run "#{sudo} cp -u /tmp/cq-quickstart-#{cq5_version}.war /usr/applications/cq5/tomcat/webapps/ROOT.war"
      run "#{sudo} chown -R cq5:cq5 /usr/applications/cq5/"    
    end

task :install_cq5_standalone, :roles => :app do
     
      run "#{sudo} smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"cq5\";get atlassian-cq5-#{cq5_version}.tar.gz /tmp/atlassian-cq5-#{cq5_version}.tar.gz;exit' "
      run "#{sudo} /bin/bash -c \"cd /tmp; tar -zxf atlassian-cq5-#{cq5_version}.tar.gz\""
      
      #run "#{sudo} /bin/bash -c \"cd /tmp/atlassian-cq5-#{cq5_version}-war;chmod +x build.sh;./build.sh\""
      
      run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5;\""
      #run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/tomcat/webapps/ROOT/;\""
      #run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/etc/repo/filesystem-mysql; mkdir -p /usr/applications/cq5/internal/cq5/etc/repo/filesystem-mysql; mkdir -p /usr/applications/cq5/external/cq5/etc/repo/filesystem-mysql\""
      
      #run "#{sudo} mkdir -p /usr/applications/cq5/plugins; mkdir -p /usr/applications/cq5/dev/cq5/plugins; mkdir -p /usr/applications/cq5/qa/cq5/plugins; mkdir -p /usr/applications/cq5/prod/cq5/plugins"
      
      #run "#{sudo} cp -u /tmp/atlassian-cq5-#{cq5_version}-war/dist-tomcat/tomcat-6/atlassian-cq5-#{cq5_version}.war /usr/applications/cq5/tomcat/webapps/ROOT.war"
      run "#{sudo} chown -R cq5:cq5 /usr/applications/cq5"    
    end
  

    task :install_cq5_war, :roles => :app do
     
      run "#{sudo} smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"cq5\";get atlassian-cq5-#{cq5_version}-war.tar.gz /tmp/atlassian-cq5-#{cq5_version}-war.tar.gz;exit' "
      run "#{sudo} smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"cq5\";get cq5-jars-tomcat-distribution-5.2-m05-tomcat-#{tomcat_version[0,1]}x.zip /tmp/cq5-jars-tomcat-distribution-5.2-m05-tomcat-#{tomcat_version[0,1]}x.zip;exit' "
      run "#{sudo} /bin/bash -c \"cd /tmp; unzip -o cq5-jars-tomcat-distribution-5.2-m05-tomcat-#{tomcat_version[0,1]}x.zip -d /usr/local/apache-tomcat-#{tomcat_version}/lib\""
      run "#{sudo} /bin/bash -c \"cd /tmp; tar -zxf atlassian-cq5-#{cq5_version}-war.tar.gz\""
      
      run "#{sudo} /bin/bash -c \"cd /tmp/atlassian-cq5-#{cq5_version}-war;chmod +x build.sh;./build.sh\""
      
      run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5;\""
      #run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/tomcat/webapps/ROOT/;\""
      #run "#{sudo} /bin/bash -c \"mkdir -p /usr/applications/cq5/etc/repo/filesystem-mysql; mkdir -p /usr/applications/cq5/internal/cq5/etc/repo/filesystem-mysql; mkdir -p /usr/applications/cq5/external/cq5/etc/repo/filesystem-mysql\""
      
      #run "#{sudo} mkdir -p /usr/applications/cq5/plugins; mkdir -p /usr/applications/cq5/dev/cq5/plugins; mkdir -p /usr/applications/cq5/qa/cq5/plugins; mkdir -p /usr/applications/cq5/prod/cq5/plugins"
      
      run "#{sudo} cp -u /tmp/atlassian-cq5-#{cq5_version}-war/dist-tomcat/tomcat-6/atlassian-cq5-#{cq5_version}.war /usr/applications/cq5/tomcat/webapps/ROOT.war"
      run "#{sudo} chown -R cq5:cq5 /usr/applications/cq5"    
    end

    task :start_tomcat_servers, :roles => :app do
      stages.each { |stage|
        set :stage_name, stage
        cq5.configure.start_tomcat_server
      }
    end

    task :restart_tomcat_servers, :roles => :app do
      stages.each { |stage|
        set :stage_name, stage
        cq5.configure.restart_tomcat_server
      }
    end

    task :install_cq5_configurations, :roles => :app do
      stages.each { |stage|
        set :stage_name, stage
        #cq5.configure.install_cq5_system_properties
        #cq5.configure.install_cq5_filesystem_mysql_repo
        #cq5.configure.install_cq5_config_xml
        #cq5.configure.install_cq5_license
        #cq5.configure.install_cq5_security_xml
        run "#{sudo} chown -R #{stage_name}:#{stage_name} /usr/applications/cq5/#{stage_name}"
        #cq5.configure.install_cq5_cq5
        #cq5.configure.install_cq5_maven
        #cq5.configure.install_cq5_subversion
      }
    end
    
    task :start_http_server, :roles => :app do
      run "#{sudo} service httpd start"
    end
    
    task :start_mysql_server, :roles => :app do
      run "#{sudo} service mysqld restart"
    end
    
    task :stop_tomcat_server, :roles => :app do
      run "#{sudo} /usr/applications/cq5/#{stage_name}/tomcat_server.sh stop 60 -force; true"
    end
    
    task :start_tomcat_server, :roles => :app do
      run "#{sudo} /usr/applications/cq5/#{stage_name}/tomcat_server.sh start"
    end

    task :restart_tomcat_server, :roles => :app do
      cq5.configure.stop_tomcat_server
      run "#{sudo} /usr/applications/cq5/#{stage_name}/tomcat_server.sh start"
    end

    task :install_apache, :roles => :app  do    
      run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install httpd"
      run "#{sudo} mkdir -p /www/logs"
      run "#{sudo} chown -R apache.apache /www/"
      run "#{sudo} chcon -R unconfined_u:object_r:httpd_log_t:s0 /www/logs"
      cq5.configure.install_httpd_conf
      stages.each { |stage|
        set :stage_name, stage
        cq5.configure.install_apache_conf
      }
      cq5.configure.install_default_site
      run "#{sudo} chkconfig --level 5 httpd on"    
      run "#{sudo} iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT"
      cq5.configure.fix_apache
    end
    
    task :yum_upgrade, :roles => :app do
      run "#{sudo} yum -y upgrade"
      run "#{sudo} yum -y install dos2unix policycoreutils-python telnet wget samba-client unzip"
    end
    
    task :fix_apache, :roles => :app do
      run "#{sudo} setsebool -P httpd_can_network_connect on; true"
    end

    task :install_ruby, :roles => :app  do
      #run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 install -y gcc-c++ patch readline readline-devel zlib zlib-devel libyaml-devel libffi-devel openssl-devel.x86_64 libxml2-devel libxslt-devel"
      run "#{sudo} yum --exclude=*.i386 --exclude=*.i586 install -y gcc-c++ patch readline readline-devel zlib zlib-devel libyaml-devel libffi-devel libxml2-devel libxslt-devel"
      run "#{sudo} bash -s stable < <(curl -k -s https://raw.github.com/wayneeseguin/rvm/cq5/binscripts/rvm-installer)"
      run "#{sudo} source /etc/profile"  
      run "#{sudo} rvm install 1.9.2"
      run "#{sudo} rvm use 1.9.2 --default"   
      run "#{sudo} rvmsudo gem install capistrano", { :shell => 'bash'}
      run "#{sudo} rvmsudo gem install railsless-deploy", { :shell => 'bash'}
      run "#{sudo} rvmsudo gem install ntlm-http; true", { :shell => 'bash'}
      run "#{sudo} rvmsudo gem install domain_name", { :shell => 'bash'}
      run "#{sudo} rvmsudo gem install webrobots", { :shell => 'bash'}
      run "#{sudo} rvmsudo gem install mechanize", { :shell => 'bash'}
    end


    task :install_server_script, :roles => :app, :except => { :no_release => true } do
      install_template server_script_template, "/usr/applications/cq5/#{stage_name}/tomcat_server.sh"
      run "#{sudo} chmod +x /usr/applications/cq5/#{stage_name}/tomcat_server.sh"
    end

    task :install_server_xml, :roles => :app, :except => { :no_release => true } do
      install_template tomcat_server_xml_template, "/usr/applications/cq5/#{stage_name}/tomcat/conf/server.xml"
    end

    task :install_httpd_conf, :roles => :app, :except => { :no_release => true } do
      install_template apache_httpd_conf_template, "/etc/httpd/conf/httpd.conf"
    end
    
    task :install_createdb, :roles => :app, :except => { :no_release => true } do
       install_template createdb_template, "/usr/applications/cq5/#{stage_name}/cq5/misc/mysql/createdb.sql"
    end

    task :install_apache_conf, :roles => :app, :except => { :no_release => true } do
      install_template File.dirname(__FILE__) + "/apache_#{stage_name}_vhost_conf.erb", "/etc/httpd/conf.d/#{stage_name}.#{site_name}.conf"
    end

    task :install_default_site, :roles => :app, :except => { :no_release => true } do
      install_template default_site_template, "/var/www/html/index.html"
    end
    
    task :install_cq5_system_properties, :roles => :app, :except => { :no_release => true } do
      install_template cq5_system_properties_template, "/usr/applications/cq5/#{stage_name}/cq5/etc/cq5.system.properties"
    end
    
    task :install_cq5_filesystem_mysql_repo, :roles => :app, :except => { :no_release => true } do
      install_template cq5_filesystem_mysql_repo_template, "/usr/applications/cq5/#{stage_name}/cq5/etc/repo/filesystem-mysql/repo.xml"
    end
    
    task :install_cq5_config_xml, :roles => :app, :except => { :no_release => true } do
      install_template cq5_config_xml_template, "/usr/applications/cq5/#{stage_name}/cq5/etc/cq5.config.import.xml"
    end
    
    task :install_cq5_security_xml, :roles => :app, :except => { :no_release => true } do
      install_template cq5_security_xml_template, "/usr/applications/cq5/#{stage_name}/cq5/etc/security.import.xml"
    end
    
    task :install_cq5_license, :roles => :app, :except => { :no_release => true } do
      install_template cq5_license_template, "/usr/applications/cq5/#{stage_name}/cq5/etc/cq5.lic"
    end

    task :install_my_cnf, :roles => :app, :except => { :no_release => true } do
      install_template my_cnf_template, "/etc/my.cnf"
    end
    

    task :install_vpnc_dfw, :roles => :app, :except => { :no_release => true } do
      install_template vpnc_dfw_template, "/etc/vpnc/dfw.conf"
    end

    task :install_vpnc_iad, :roles => :app, :except => { :no_release => true } do
      install_template vpnc_iad_template, "/etc/vpnc/iad.conf"
    end

    def install_template(template_name,location)
      template = File.read("#{template_name}")
      buffer = ERB.new(template).result(binding)
      basename= File.basename(location)
      put buffer, "/tmp/#{basename}"
      run "#{sudo} mv /tmp/#{basename} #{location}"
      run "#{sudo} dos2unix #{location}"
    end
    
    
  end
end 
