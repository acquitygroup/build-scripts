

namespace :deploy do 
  task :finalize_update, :except => { :no_release => true } do
    #
  end

  task :cold do
    setup
    start
  end

  task :restart, roles => :app, :except => { :no_release => true } do
   
  end

  task :setup do
    cq5.configure.fix_sudoers_for_cap
    cq5.configure.set_hostname
    cq5.configure.yum_upgrade
    #cq5.configure.install_ruby
    cq5.configure.install_java
    cq5.configure.install_application_directories
    #cq5.configure.install_tomcat
    #cq5.configure.install_mysql_connector
    #cq5.configure.install_cq5
    cq5.configure.install_apache
    #cq5.configure.install_mysql
    #cq5.configure.start_mysql_server
    #cq5.configure.configure_mysql    
    #cq5.configure.install_cq5_configurations   
   
  end

  task :start do
    cq5.configure.restart_tomcat_servers
    cq5.configure.start_http_server
  end

  task :rotatekeys do
    cq5.configure.rotate_keys
  end

end
