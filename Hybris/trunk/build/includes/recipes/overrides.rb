=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Capsitrano Hybris overrides (overrides.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and Confidential
=end

default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    update
    restart
    cleanup
  end

  task :build do
    hybris.build.build
  end

  task :cold do
    setup
    start
  end

  task :endeca do
    update_endeca
    restart_endeca
  end

  task :restart, :except => { :no_release => true } do
    hybris.util.restart    
  end

  task :restart_endeca, :except => { :no_release => true } do
    hybris.deploy.restart_endeca
  end

  task :update_endeca  do
    transaction do
      update_code_endeca
      #create_symlink
    end
  end

  task :update_code, :except => { :no_release => true }  do
    #On rollback has never been useful, just fail.
    #on_rollback { run "rm -rf #{release_path}; true"; restart }
    #hybris.deploy.update_base    
    hybris.util.start_vpn    
    hybris.deploy.fix_packages
    hybris.deploy.create_release_path
    hybris.deploy.copy_config
    hybris.deploy.copy_extensions    
    hybris.deploy.copy_platform
    if use_coherence then
      hybris.deploy.copy_coherence
    end
    finalize_update    
  end

   task :update_code_endeca, :except => { :no_release => true }  do
    on_rollback { restart_endeca }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    hybris.deploy.copy_endeca
    finalize_update_endeca
  end

 

  task :finalize_update do
    hybris.deploy.detect_java_home
    #hybris.deploy.stop_hybris   
    hybris.deploy.deploy_platform
    hybris.deploy.deploy_extensions
    hybris.deploy.deploy_config
    if use_cybersource then
      hybris.deploy.deploy_cybersource
    end
    if use_coherence then
      hybris.deploy.deploy_coherence
    end
    hybris.deploy.setup_config
    hybris.deploy.link_hybris
    hybris.deploy.ant_server
    hybris.deploy.fix_tcserver
    hybris.deploy.install_version_check
    if use_coherence then    
      hybris.deploy.build_coherence
    end    
    #hybris.deploy.install_tcserver
    #cleanup
  end

  task :after_restart do    
    hybris.util.stop_vpn    
  end
  
   task :finalize_update_endeca do
    hybris.deploy.deploy_endeca
    hybris.deploy.setup_endeca
    #cleanup
      
   end

  task :setup do
    set :user, "root"
    #hybris.configure.fix_sudoers_for_cap
    #hybris.configure.install_audit_tools
    #hybris.configure.set_hostname
    #hybris.configure.install_svn_directories
    #hybris.configure.install_apache
    #hybris.configure.install_hybris
    #hybris.configure.install_deploy_keys
    #hybris.configure.install_viewvc
    #hybris.configure.sync_svn_conf
    hybris.configure.install_fuse
    hybris.configure.mount_fuse
    hybris.configure.start_http_server
    hybris.configure.install_cron_scripts
    hybris.configure.install_global_hooks
    hybris.configure.install_master_symlinks
    hybris.configure.install_slave_symlinks
    hybris.configure.start_cron
  end

  task :start do
    hybris.configure.start_http_server
  end

  task :rotatekeys do
    hybris.configure.rotate_keys
  end

  task :create_symlink, :except => { :no_release => true } do
    on_rollback do
      if previous_release       
        if fetch(:os_system, "linux") == "windows"
          run "rm -f #{current_path}; cd #{deploy_to}; export TARGET=`cygpath -wa #{previous_release}`;cmd /c mklink /d current $TARGET; true"
        else
          run "#{try_sudo} rm -f #{current_path}; #{try_sudo} ln -s #{previous_release} #{current_path}; true"
        end
      else
        logger.important "no previous release to rollback to, rollback of symlink skipped"
      end
    end
  
    if fetch(:os_system, "linux") == "windows"
      run "rm -f #{current_path}; cd #{deploy_to}; export TARGET=`cygpath -wa #{latest_release}`;cmd /c mklink /d current $TARGET"
    else
      run "#{try_sudo} rm -f #{current_path} && #{try_sudo} ln -s #{latest_release} #{current_path}"
    end
  end
  
  
end

after 'deploy:restart', "deploy:after_restart"