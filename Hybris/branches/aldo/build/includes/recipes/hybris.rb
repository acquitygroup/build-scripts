=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Hybris loader (hybris.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and Confidential
=end

load File.dirname(__FILE__) + '/hybris/configure'
load File.dirname(__FILE__) + '/hybris/build'
load File.dirname(__FILE__) + '/hybris/deploy'
load File.dirname(__FILE__) + '/hybris/util'
set :tomcat_version, "7.0.27"

namespace :hybris do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 

alias :acq_upload :upload
#alias :acq_wave_restart :restart_hybris

if File.exist?(File.dirname(__FILE__) + '/acquity/extras.rb')
  load File.dirname(__FILE__) + '/acquity/extras'
end

