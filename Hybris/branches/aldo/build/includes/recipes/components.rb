=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Hybris components (components.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and Confidential
=end

set :application, "hybris"
