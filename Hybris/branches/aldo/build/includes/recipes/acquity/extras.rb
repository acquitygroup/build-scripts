=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Hybris FULL License extras (extras.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and confidential
  * Use of this file requires license terms and support retainer between Acquity Group/Accenture Interactive and Licensee
=end

namespace :acquity do

  namespace :util do
   
    task :start_vpn do
      print "Checking for running VPN to #{vpn_datacenter}.\n"
      vpncheck = `ps -ef | grep vpnc | grep root`
      if vpncheck.include? "vpnc #{vpn_datacenter}" then
        print "VPN to #{vpn_datacenter} Running, skipping."
      else
        print "Starting VPN to #{vpn_datacenter}.\n"
        system "sudo /usr/local/sbin/vpnc #{vpn_datacenter}"
        if $?.exitstatus != 0 then
          raise "VPN Start Failure!"
        end
      end
    end

    task :stop_vpn do
      print "Stopping VPN.\n"
      system "sudo /usr/local/sbin/vpnc-disconnect"
      if $?.exitstatus != 0 then
        raise "VPN Stop Failure!"
      end
    end

    task :fix_keys do
      run "cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys", :hosts => fetch(:gateway,"")
      find_servers_for_task(current_task).each do |hostname|
        puts "=> run mytask on #{hostname}"
        run "scp -o StrictHostKeyChecking=no ~/.ssh/id_rsa #{user}@#{hostname}:~/.ssh/id_rsa", :hosts => fetch(:gateway,"")        
        run "scp -o StrictHostKeyChecking=no ~/.ssh/id_rsa.pub #{user}@#{hostname}:~/.ssh/id_rsa.pub", :hosts => fetch(:gateway,"")             
      end
    end
  end
end



before "acquity:hybris:util:fetch_logs", "acquity:hybris:util:start_vpn"
after "acquity:hybris:util:fetch_logs", "acquity:hybris:util:stop_vpn"

before "acquity:hybris:util:initialize_hybris", "acquity:hybris:util:start_vpn"
before "acquity:hybris:util:initialize_hybris", "hybris:util:detect_java_home"
before "acquity:hybris:util:initialize_hybris", "hybris:deploy:stop_hybris"
after "acquity:hybris:util:initialize_hybris", "hybris:deploy:restart_hybris"
after "acquity:hybris:util:initialize_hybris", "acquity:hybris:util:stop_vpn"

before "acquity:hybris:util:update_hybris", "acquity:hybris:util:start_vpn"
before "acquity:hybris:util:update_hybris", "hybris:util:detect_java_home"
before "acquity:hybris:util:update_hybris", "hybris:deploy:stop_hybris"
after "acquity:hybris:util:update_hybris", "hybris:deploy:restart_hybris"
after "acquity:hybris:util:update_hybris", "acquity:hybris:util:stop_vpn"

before "acquity:hybris:util:migrate", "acquity:hybris:util:start_vpn"
after "acquity:hybris:util:migrate", "hybris:deploy:restart_hybris"
after "acquity:hybris:util:migrate", "acquity:hybris:util:stop_vpn"

before "acquity:hybris:util:sanitize", "acquity:hybris:util:start_vpn"
before "acquity:hybris:util:sanitize", "hybris:deploy:stop_hybris"
after "acquity:hybris:util:sanitize", "hybris:deploy:restart_hybris"
after "acquity:hybris:util:sanitize", "acquity:hybris:util:stop_vpn"


namespace :acquity do
  namespace :hybris do
    namespace :util do
    
      
      task :start_vpn do
        if fetch(:use_vpn, false) == true then
          acquity.util.start_vpn
        end
      end  
    
      task :stop_vpn do
        if fetch(:use_vpn, false) == true then
          acquity.util.stop_vpn
        end
      end    
    
      task :test_admin, :roles => :app do
        servers = find_servers :roles => :app, :only => { :admin => true }     
        if servers.empty?
          run "echo 'I am Admin Server (Once)!'", :once => true     
        else
          run "echo 'I am Admin Server (First Flagged)!'", :hosts => servers[0].host   
        end
      end
    

      task :initialize_hybris, :roles => :app do
        print "Sleeping 60 seconds.\n"
        sleep 60 
        servers = find_servers :roles => :app, :only => { :admin => true }     
        if servers.empty?
          run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant initialize", :once => true     
        else
          run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant initialize", :hosts => servers[0].host
        end
        run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} start"
      end      
   
      task :update_hybris, :roles => :app do  
        print "Sleeping 60 seconds.\n"
        sleep 60       
        servers = find_servers :roles => :app, :only => { :admin => true }     
        if servers.empty?
          run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant updatesystem", :once => true     
        else
          run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant updatesystem", :hosts => servers[0].host    
        end
        run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} start"
      end
      
      
      task :fetch_logs, :roles => :app do   
        print "Fetching Logs from #{stage_name}.\n"
        dirname = File.absolute_path(File.dirname(__FILE__))
        system "mkdir -p '#{dirname}/../../../../../Logs/#{stage_name}'"
        download("#{deploy_to}/log","#{dirname}/../../../../../Logs/#{stage_name}/$CAPISTRANO:HOST$/",:via=> :scp, :recursive => true)
      end
    
      task :migrate, :roles => :app, :on_error => :continue do
        run "cd #{deploy_to}/hybris; cp -R temp #{deploy_to}"
        run "cd #{deploy_to}/hybris; cp -R log #{deploy_to}"
        run "cd #{deploy_to}/hybris; cp -R data #{deploy_to}"
        run "cd #{deploy_to}/hybris; cp -R certs #{deploy_to}"
        run "mkdir -p #{deploy_to}/temp"
        run "mkdir -p #{deploy_to}/log"
        run "mkdir -p #{deploy_to}/data"
      end
    
      task :sanitize, :roles => :app, :on_error => :continue do
        run "cd #{deploy_to}; rm -rf hybris"
        run "cd #{deploy_to}; rm -rf current"
        run "cd #{deploy_to}; rm -rf releases"
        run "cd #{deploy_to}; rm -rf log"
        run "cd #{deploy_to}; rm -rf data"
        run "cd #{deploy_to}; rm -rf temp"
        run "mkdir -p #{deploy_to}/temp"
        run "mkdir -p #{deploy_to}/log"
        run "mkdir -p #{deploy_to}/data"
      end
      
      task :wave_restart, :roles => :app do
        if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
          set :tcserver_dir, "tcServer"       
        else
          set :tcserver_dir, "tcServer-6.0"
        end
        servers = find_servers_for_task(current_task)       
        servers.each do |server|     
          begin
            if fetch(:os_system, "linux") == "windows"
              run "chmod 755 #{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}/tcruntime-ctl.bat; true", :hosts => server.host        
              run "cd '#{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} stop; true" , :hosts => server.host
            else
              run "chmod 755 #{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}/tcruntime-ctl.sh; true", :hosts => server.host
              run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} stop; true", :hosts => server.host
            end
          rescue
            p "Error happened with stopping hybris. Continuing on..."           
          end
          print "Sleeping 10 seconds. (Ensuring Clean Shutdown.)\n"
          sleep 10
          if fetch(:os_system, "linux") == "windows"
            run "cd '#{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} start; true" , :hosts => server.host
          else
            run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} start", :hosts => server.host
          end
          #run "tail -f '#{deploy_to}/current/hybris/log/tcServer/#{stage_name}/catalina.out' | grep 'Server startup in' | read -t #{fetch(:restart_timeout, "300")} dummy_var ", :hosts => server.host
          #run "timeout #{fetch(:restart_timeout, "300")} tail -f '#{deploy_to}/current/hybris/log/tcServer/#{stage_name}/catalina.out' | grep -q --line-buffered 'Server startup in' ", :hosts => server.host
          run "timeout #{fetch(:restart_timeout, "300")} grep -q --line-buffered 'Server startup in' <(tail -f '#{deploy_to}/current/hybris/log/tcServer/#{stage_name}/catalina.out') ", :hosts => server.host
          print "Sleeping 20 seconds. (Ensuring Restarted Node Can Catchup.)\n"
          sleep 20
        end
      end   
      
    end
  end
end

def acq_upload(from, to, options={}, &block)
  case fetch(:deploy_via, "copy")
  when "murder"
  when "rsync"
    puts "Implementing rsync deploy"
    servers = find_servers_for_task(current_task)
    upload(from,to,options.merge(:hosts => servers[0].host).merge(:encryption => 'arcfour'), &block)
    set :first_server, servers[0].host
    parallel do |session|
      servers.each_with_index do |server, idx|
        if(idx==0)
          cmd = ""
        else 
          cmd = "bash -c 'sleep $[ RANDOM % 5 ]; scp -c arcfour -o StrictHostKeyChecking=no #{user}@#{servers[0].host}:#{to} #{to}'"
        end      
        session.when "server.host == '#{server.host}'", cmd
      end
    end      
  when "jump"
    puts "Implementing jump deploy"
    gateway_server = fetch(:gateway,"")
    run "mkdir -p '~#{release_path}'", :hosts => gateway_server
    upload(from, "~#{to}",:hosts => gateway_server, &block)
    run "scp -c arcfour -o StrictHostKeyChecking=no #{user}@#{gateway_server}:~#{to} '#{to}'"    
  else
    upload(from, to, options={}, &block)
  end
end
    