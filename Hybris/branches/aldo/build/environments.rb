desc "deploy to master environment"
task :master do
  set :stage_name, "master"

end

desc "deploy to development environment"
task :dev do
  set :stage_name, "dev"
  set :site_name, "dev.acq.acq"
  set :username, "hybris"
  set :deploy_to, "/usr/applications/hybris-install"
  role :app, "10.50.30.29"
  role :web, "10.50.30.29"
end



desc "deploy to qa environment"
task :qa do
  set :stage_name, "qa"
  set :site_name, "qa.acq.acq"
  set :username, "acq"
  set :deploy_to, "/usr/applications/ag-commerce-acq-qa"
  role :app, "10.50.40.60"
  role :app, "10.50.40.61"
  role :web, "10.50.40.60"
  role :web, "10.50.40.61"
end

desc "UAT Environment"
task :UAT do
  set :stage_name, "uat"
  set :site_name, "uat.acq.acq"
  #role :app, "172.26.48.58"
  #role :web, "172.26.48.58"
end

desc "deploy to prod environment"
task :prod do
  set :stage_name, "prod"

end