default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    update
    restart
  end

  task :build do
    hybris.build.build
  end

  task :cold do
    setup
    start
  end

  task :endeca do
    update_endeca
    restart_endeca
  end

  task :restart, :except => { :no_release => true } do
    hybris.deploy.restart_hybris
  end

  task :restart_endeca, :except => { :no_release => true } do
    hybris.deploy.restart_endeca
  end

  task :update_endeca  do
    transaction do
      update_code_endeca
      #create_symlink
    end
  end

  task :update_code, :except => { :no_release => true }  do
    on_rollback { run "rm -rf #{release_path}; true"; restart }
    #hybris.deploy.update_base
    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    hybris.deploy.fix_packages
    hybris.deploy.create_release_path
    hybris.deploy.copy_config
    hybris.deploy.copy_extensions    
    hybris.deploy.copy_platform
    if use_coherence then
      hybris.deploy.copy_coherence
    end
    finalize_update    
  end

   task :update_code_endeca, :except => { :no_release => true }  do
    on_rollback { restart_endeca }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    hybris.deploy.copy_endeca
    finalize_update_endeca
  end

 

  task :finalize_update do
    hybris.deploy.detect_java_home
    #hybris.deploy.stop_hybris   
    hybris.deploy.deploy_platform
    hybris.deploy.deploy_extensions
    hybris.deploy.deploy_config
    if use_cybersource then
      hybris.deploy.deploy_cybersource
    end
    if use_coherence then
      hybris.deploy.deploy_coherence
    end
    hybris.deploy.setup_config
    hybris.deploy.link_hybris
    hybris.deploy.ant_server
    hybris.deploy.fix_tcserver
    hybris.deploy.install_version_check
    if use_coherence then    
      hybris.deploy.build_coherence
    end    
    #hybris.deploy.install_tcserver
    cleanup
  end

  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update_endeca do
    hybris.deploy.deploy_endeca
    hybris.deploy.setup_endeca
    #cleanup
      
   end

  task :setup do
    set :user, "root"
    #hybris.configure.fix_sudoers_for_cap
    #hybris.configure.install_audit_tools
    #hybris.configure.set_hostname
    #hybris.configure.install_svn_directories
    #hybris.configure.install_apache
    #hybris.configure.install_hybris
    #hybris.configure.install_deploy_keys
    #hybris.configure.install_viewvc
    #hybris.configure.sync_svn_conf
    hybris.configure.install_fuse
    hybris.configure.mount_fuse
    hybris.configure.start_http_server
    hybris.configure.install_cron_scripts
    hybris.configure.install_global_hooks
    hybris.configure.install_master_symlinks
    hybris.configure.install_slave_symlinks
    hybris.configure.start_cron
  end

  task :start do
    hybris.configure.start_http_server
  end

  task :rotatekeys do
    hybris.configure.rotate_keys
  end

  task :symlink, :except => { :no_release => true } do
    on_rollback do
      if previous_release        
        run "rm -f #{current_path}; cd #{deploy_to}; export TARGET=`cygpath -wa #{previous_release}`;cmd /c mklink /d current $TARGET; true"
      else
        logger.important "no previous release to rollback to, rollback of symlink skipped"
      end
    end

    run "rm -f #{current_path}; cd #{deploy_to}; export TARGET=`cygpath -wa #{latest_release}`;cmd /c mklink /d current $TARGET"
  end

  task :cleanup, :except => { :no_release => true } do
    count = fetch(:keep_releases, 5).to_i
    local_releases = capture("/usr/bin/ls -xt #{releases_path}").split.reverse
    if count >= local_releases.length
      logger.important "no old releases to clean up"
    else
      logger.info "keeping #{count} of #{local_releases.length} deployed releases"
      directories = (local_releases - local_releases.last(count)).map { |release|
        File.join(releases_path, release) }.join(" ")

      try_sudo "rm -rf #{directories}"
    end
  end


end

after 'deploy:restart', "deploy:after_restart"