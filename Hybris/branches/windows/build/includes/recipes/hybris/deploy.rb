namespace :hybris do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  namespace :deploy do

    task :create_release_path, :roles => :app do
      run "mkdir -p #{release_path}"
    end

    task :detect_java_home, :roles => :app do
      if remote_file_exists?("/usr/applications/java/current") then
        java_home = "/usr/applications/java/current"
      elsif remote_file_exists?("/usr/java/latest") then
        java_home = "/usr/java/latest"
      else
        java_home = "C:\\Program Files\\Java\\#{jdk_version}"
      end
      set :java_home, "#{java_home}"
      set :java_home_cmd, "export JAVA_HOME='#{java_home}'"
    end
    
    task :fix_packages do
      dirname = File.absolute_path(File.dirname(__FILE__))
      run_locally "cd '#{dirname}/../../../../../Packages'; mv ./*/*/* .;  find . -type d  ! -name . -prune -print0 | xargs -0 rm -rf"
    end
    
    task :copy_config, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../../Packages/hybrisServer-Config.tgz", "#{release_path}/hybrisServer-Config.tgz"
    end

    task :copy_coherence, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      if use_coherence then
        upload "#{dirname}/../../../../../Packages/hybrisServer-Coherence.tgz", "#{release_path}/hybrisServer-Coherence.tgz"
      end
    end

    task :copy_endeca, :roles => :endeca do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../../Packages/hybrisServer-Endeca.zip", "#{deploy_to}/hybrisServer-Endeca.zip"
    end

    task :copy_extensions, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../../Packages/hybrisServer-AllExtensions.zip", "#{release_path}/hybrisServer-AllExtensions.zip"
    end

    task :copy_platform, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      if platform_update or remote_file_exists?("#{deploy_to}/hybrisServer-Platform.zip") != true then
        upload "#{dirname}/../../../../../Packages/hybrisServer-Platform.zip", "#{deploy_to}/hybrisServer-Platform.zip"
      end
    end


    task :copy_extensions, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../../Packages/hybrisServer-AllExtensions.zip", "#{release_path}/hybrisServer-AllExtensions.zip"
    end

    task :deploy_platform, :roles => :app do
      run "unzip '#{deploy_to}/hybrisServer-Platform.zip' -d #{release_path}/"
    end

    task :deploy_extensions, :roles => :app do
      run "unzip '#{release_path}/hybrisServer-AllExtensions.zip' -d #{release_path}/"
      # keep extensions artifact.
      #run "rm -f '#{release_path}/hybrisServer-AllExtensions.zip'"
    end

    task :deploy_config, :roles => :app do
      run "tar -zxvf '#{release_path}/hybrisServer-Config.tgz' -C #{release_path}/hybris/"
    end

    task :deploy_cybersource, :roles => :app do
      run "rm -f #{release_path}/hybris/config/cybs.properties"
      run "cd #{release_path}/hybris/config; cmd /c mklink cybs.properties cybs#{stage_name}.properties"
    end

    task :deploy_endeca, :roles => :endeca do
      run "unzip -o '#{deploy_to}/hybrisServer-Endeca.zip' -d #{deploy_to}/endeca/"
    end

    task :deploy_coherence, :roles => :app do
      run "tar -zxvf '#{release_path}/hybrisServer-Coherence.tgz' -C #{release_path}/hybris/"
    end

    task :setup_config, :roles => :app do
      run "rm -f #{release_path}/hybris/config/local.properties"
      run "cd #{release_path}/hybris/config; cmd /c mklink local.properties local#{stage_name}.properties"
      config_rev = capture("/usr/bin/grep 'Changed Rev' #{release_path}/hybris/config/revision.txt | cut -c19-").strip
      extension_rev = capture("/usr/bin/grep 'Changed Rev' #{release_path}/hybris/bin/custom/revision.txt | cut -c19-").strip
      platform_rev = capture("/usr/bin/grep 'Changed Rev' #{release_path}/hybris/bin/platform/revision.txt | cut -c19-").strip
      revision = "Extension CR:#{extension_rev},Config CR:#{config_rev},Platform CR:#{platform_rev}"
      print "Setting up local.properties file.\n"
      run "/usr/bin/sed -i 's/build.revision=BUILD REVISION NUMBER GOES HERE/build.revision=#{revision}/g' #{release_path}/hybris/config/local.properties"
      if exists?(:db_password) then
        run "/usr/bin/sed -i 's/DB_PASSWORD/#{db_password}/g' #{release_path}/hybris/config/local.properties"
      end
      if exists?(:celum_password) then
        run "/usr/bin/sed -i 's/CELUM_PASSWORD/#{celum_password}/g' #{release_path}/hybris/config/local.properties"
      end
      if exists?(:ftp_password) then
        run "/usr/bin/sed -i 's/FTP_PASSWORD/#{ftp_password}/g' #{release_path}/hybris/config/local.properties"
      end
      
      servers = find_servers_for_task(current_task)
      servers.each_with_index do |s,index|
        puts "Index is #{index}"
        clusterid = index + 16
        instanceid = index + 1
        maxid = servers.size
        run "/usr/bin/sed -i 's/HOST-IP/$CAPISTRANO:HOST$/g' #{release_path}/hybris/config/local.properties", :hosts => s
        run "/usr/bin/sed -i 's/bundled.tcserver.instance=TCINSTANCEID/bundled.tcserver.instance=#{stage_name}-#{client_shortcode}-#{instanceid}/g' #{release_path}/hybris/config/local.properties", :hosts => s
        run "cd #{release_path}/hybris/bin/platform/tcServer-6.0/; export TARGET=`cygpath -wa #{release_path}/hybris/bin/platform/tcServer-6.0/#{stage_name}-#{client_shortcode}-#{instanceid}`; cmd /c mklink /D #{stage_name} $TARGET", :hosts => s
        run "/usr/bin/sed -i 's/cluster.id=CLUSTERID/cluster.id=#{clusterid}/g' #{release_path}/hybris/config/local.properties", :hosts => s
        run "/usr/bin/sed -i 's/cluster.maxid=MAXID/cluster.maxid=#{maxid}/g' #{release_path}/hybris/config/local.properties", :hosts => s
      end

      if remote_file_exists?("#{release_path}/hybris/config/tcServer/conf") then
        run "mv #{release_path}/hybris/config/tcServer/conf #{release_path}/hybris/config/tcServer/default"
      end
      if remote_file_exists?("#{release_path}/hybris/config/tcServer/#{stage_name}")
        run "cd #{release_path}/hybris/config/tcServer/; export TARGET=`cygpath -wa #{release_path}/hybris/config/tcServer/#{stage_name}`; cmd /c mklink /D conf $TARGET"
      elsif remote_file_exists?("#{release_path}/hybris/config/tcServer/final")
        run "cd #{release_path}/hybris/config/tcServer/; export TARGET=`cygpath -wa #{release_path}/hybris/config/tcServer/prod`; cmd /c mklink /D conf $TARGET"
      else
        run "cd #{release_path}/hybris/config/tcServer/; export TARGET=`cygpath -wa #{release_path}/hybris/config/tcServer/default`; cmd /c mklink /D conf $TARGET"
      end
    end

    task :link_hybris, :roles => :app do
      run "cd #{release_path}/hybris; export TARGET=`cygpath -wa #{deploy_to}/temp`;cmd /c mklink /D temp $TARGET"
      run "cd #{release_path}/hybris; export TARGET=`cygpath -wa #{deploy_to}/data`;cmd /c mklink /D data $TARGET"
      run "cd #{release_path}/hybris; export TARGET=`cygpath -wa #{deploy_to}/log`;cmd /c mklink /D log $TARGET"
      run "/usr/bin/sed -i 's/$1 run/$1 $2/' #{release_path}/hybris/bin/platform/tcserver.sh;"
    end

     task :setup_endeca, :roles => :endeca do
       run "find #{deploy_to} -name *.sh | xargs chmod 744 "
     end

   task :ant_server, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      #Wrapper for temp directory issues.
      run "rm -rf #{release_path}/hybris/temp; mkdir #{release_path}/hybris/temp; true"
      run "cd '#{release_path}/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant clean customize all"
      run "rm -rf #{release_path}/hybris/temp;"
      run "cd #{release_path}/hybris; export TARGET=`cygpath -wa #{deploy_to}/temp`;cmd /c mklink /D temp $TARGET"
      #One day, it will be this simple.
      #run "cd '#{release_path}/hybris/bin/platform'; source ./setantenv.sh ; ant server"
    end

    task :build_coherence, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      run "cd '#{release_path}/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant buildCoherence"
    end

    task :install_tcserver, :roles => :app do
      run "chmod 755 #{release_path}/hybris/bin/platform/tcServer-6.0/tcruntime-instance.sh"
      run "cd #{release_path}/hybris/bin/platform/tcServer-6.0; #{java_home_cmd}; ./tcruntime-instance.sh create #{stage_name}"
    end

    task :fix_tcserver, :roles => :app do
      #Nuke ROOT directory to fix context paths.
      run "cd #{release_path}/hybris/bin/platform/tcServer-6.0/#{stage_name}/webapps; rm -rf ROOT"
      # comment out tcserver java_opts, we run the memory show.
      run "/usr/bin/sed -i 's/^JVM_OPTS/#JVM_OPTS/' #{release_path}/hybris/bin/platform/tcServer-6.0/#{stage_name}/bin/setenv.bat"
      #Fix tcserver log location to outside release path.
      run "rm -rf '#{release_path}/hybris/bin/platform/tcServer-6.0/#{stage_name}/logs'"
      run "cd #{release_path}/hybris/bin/platform/tcServer-6.0/#{stage_name}/; export TARGET=`cygpath -wa #{release_path}/hybris/log/tcServer/#{stage_name}`;cmd /c mklink /D logs $TARGET"
      # Added logs symlink for operations.
      run "cd #{release_path}/hybris/bin/platform/;export TARGET=`cygpath -wa #{release_path}/hybris/bin/platform/tcServer-6.0/#{stage_name}/logs`;cmd /c mklink /D logs $TARGET"
      #Make sure stage name log dir exists
      run "mkdir -p #{deploy_to}/log/tcServer/#{stage_name}"
    end

    task :install_version_check, :roles => :app do
      #TODO Add code to display CR versions and java version in html or jsp file.
    end

    task :stop_hybris, :roles => :app, :on_error => :continue do
      run "cd '#{deploy_to}/current/hybris/bin/platform/tcServer-6.0'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} stop"
    end

    task :restart_hybris, :roles => :app do
      run "chmod 755 #{deploy_to}/current/hybris/bin/platform/tcServer-6.0/tcruntime-ctl.bat"
      run "chmod +x #{deploy_to}/current/hybris/bin/platform/tcserver.bat"
      run "cd '#{deploy_to}/current/hybris/bin/platform/tcServer-6.0'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} reinstall"
      hybris.deploy.stop_hybris
      print "Sleeping 60 seconds.\n"
      sleep 60
      run "cd '#{deploy_to}/current/hybris/bin/platform/tcServer-6.0'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} start"
    end

    task :restart_endeca, :roles => :endeca do
      #Do Nothing
    end

    task :migrate, :roles => :app, :on_error => :continue do
      run "cd #{deploy_to}/hybris; cp -R temp #{deploy_to}"
      run "cd #{deploy_to}/hybris; cp -R log #{deploy_to}"
      run "cd #{deploy_to}/hybris; cp -R data #{deploy_to}"
      run "cd #{deploy_to}/hybris; cp -R certs #{deploy_to}"
      run "mkdir -p #{deploy_to}/temp"
      run "mkdir -p #{deploy_to}/log"
      run "mkdir -p #{deploy_to}/data"
    end

    task :sanitize, :roles => :app, :on_error => :continue do
      run "cd #{deploy_to}; rm -rf hybris"
      run "cd #{deploy_to}; rm -rf current"
      run "cd #{deploy_to}; rm -rf releases"
      run "cd #{deploy_to}; rm -rf log"
      run "cd #{deploy_to}; rm -rf data"
      run "cd #{deploy_to}; rm -rf temp"
      run "mkdir -p #{deploy_to}/temp"
      run "mkdir -p #{deploy_to}/log"
      run "mkdir -p #{deploy_to}/data"
    end
    
  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
