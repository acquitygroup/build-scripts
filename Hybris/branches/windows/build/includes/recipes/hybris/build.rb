namespace :hybris do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do


    task :install_base do
      dirname = File.absolute_path(File.dirname(__FILE__))
      puts dirname
	  
      system "mkdir -p '#{dirname}/../../../../../HybrisBase'"
      system "mkdir -p '#{dirname}/../../../../../Packages'"
      path = "#{dirname}/../../../../../HybrisBase/#{hybris_version}"
      if File.exists?("#{path}/hybris") && File.directory?("#{path}/hybris")
        puts "Base Hybris Install Exists"
      else
        puts "Copying Base Hybris Install"
        system "mkdir -p '#{path}'"
        
        #system "smbclient //srv-ag2/software/ #{password} -U #{username} -c 'cd \"Hybris\";get hybris-multichannel-suite-#{hybris_version}.zip \"#{path}/hybris-multichannel-suite-#{hybris_version}.zip\";exit' "
        system "cp 'C:/hybris_binaries/hybris-multichannel-suite-#{hybris_version}.zip' '#{path}'"
		system "cd '#{path}'; unzip hybris-multichannel-suite-#{hybris_version}.zip"
        system "rm -f '#{path}/hybris-multichannel-suite-#{hybris_version}.zip'"
      end
      system "cp -Rnv '#{path}/hybris/bin' '#{dirname}/../../../../' "
      puts "mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      system "mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      puts "cd '#{dirname}/../../../../bin/platform'; source setantenv.sh ; ant server -Dinput.template=develop"
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant server -Dinput.template=develop"
      puts "cp -Rnv '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      system "cp -Rnv  '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      system "rm -rf '#{dirname}/../../../../config' "
      system "mv '#{dirname}/../../../../config-temp' '#{dirname}/../../../../config' "
	  
	  system "cp -Rnv '#{dirname}/../../../../custom/'* '#{dirname}/../../../../bin/custom' "
    end

    task :clean_packages do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "rm -rf '#{dirname}/../../../../../Packages/*'"
    end

    task :ant_build do
      dirname = File.absolute_path(File.dirname(__FILE__))
	  
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant build"
      if $?.exitstatus != 0 then
        raise "Build Failure!"
      end
    end

    task :ant_clean do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant clean"
       if $?.exitstatus != 0 then
        raise "Clean Failure!"
      end
    end

    task :ant_customize do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant customize"
    end

    task :ant_package do
       dirname = File.absolute_path(File.dirname(__FILE__))
      if ENV['U_WORKSPACE'] 
        workspace = ENV['U_WORKSPACE']      
      elsif ENV['WORKSPACE'] 
        workspace = ENV['WORKSPACE']
      else
        workspace = "#{dirname}/../../../../.."
      end
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant production -Dproduction.output.path='#{workspace}/Packages'"
      system "cd '#{dirname}/../../../../temp/'; zip ../../Packages/hybrisServer-AllExtensions.zip hybris/bin/custom/revision.txt"
    end

    task :extensions_package do
      dirname = File.absolute_path(File.dirname(__FILE__))
      exclusions = ["platform",".svn","doc/*-src.zip","doc/*-sources.jar","doc/resources/*.mdl","doc/install.xml",
        "eclipsebin/*","extensioninfo.xsd","macrotests/*","resources/items.xsd","src/*", "testsrc/*,",
        "temp/*","gensrc/*", "testsrc/*","testclasses/*","classes/*", "hmc/resources/hmc.xsd",
        "hmc/src/*", "hmc/classes/*", "web/src/*",  "build.xml", "platformhome.properties", ".settings/*"]
      tarstring = ""
      exclusions.each {|exclusion| tarstring = tarstring + " --exclude='#{exclusion}" }
      system "cd '#{dirname}/../../../../bin/'; tar -cvzf '#{dirname}/../../../../../Packages/hybrisServer-AllExtensions.tgz' #{tarstring} *"
	  #system "cp '#{dirname}/../../../../../Packages/*' '/apps/Jenkins/jobs/Belkin-Build-AZDev2/workspace/Packages'"
    end

    task :platform_package do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "cd '#{dirname}/../../../../bin/'; tar -cvzf '#{dirname}/../../../../../Packages/hybrisServer-Platform.tgz' --exclude='.svn' platform"
    end

    task :config_package do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "cd '#{dirname}/../../../../'; tar -cvzf '#{dirname}/../../../../../Packages/hybrisServer-Config.tgz' --exclude='.svn' config"
	  #system "cp '#{dirname}/../../../../../Packages/*' '/apps/Jenkins/jobs/Belkin-Build-AZDev2/workspace/Packages'"
    end

    task :install_revisiontxt do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../bin/platform'/revision.txt"
      system "cd '#{dirname}/../../../../config'; svn info > '#{dirname}/../../../../config'/revision.txt"
      system "cd '#{dirname}/../../../../bin/custom'; svn info  > '#{dirname}/../../../../bin/custom'/revision.txt"
      system "mkdir -p '#{dirname}/../../../../temp/hybris/bin/custom'; cp -f '#{dirname}/../../../../bin/custom'/revision.txt '#{dirname}/../../../../temp/hybris/bin/custom' "
    end

    task :create_sonar_properties do
      set :project_key, "#{client_shortcode}.acquity:Hybris_#{project_code}"
      job_name = "#{client_code} Hybris #{project_code}"
      if ENV['JOB_NAME'] then
        job_name = ENV['JOB_NAME']
      end
      set :project_job, "#{job_name}"
      revision = "0"
      if ENV['SVN_REVISION'] then
        revision = ENV['SVN_REVISION']
      end
      set :revision, "#{revision}"
      workspace = File.absolute_path(File.dirname(__FILE__)) + "/../../../../.."
      if ENV['WORKSPACE'] then
        workspace = ENV['WORKSPACE']
      end
      svn_url = "http://"
      if ENV['SVN_URL'] then
        svn_url = ENV['SVN_URL']
      end
      #sources=`find '#{workspace}/source/bin/custom' -type d -name src | grep -v /web/src | grep -v /hmc/src`
      #list local extensions, only take lines containing /custom/
      #Use cut to seperate the fields by ", take the second field
      #Replace what is before /custom with the absolute path.
      #sources=`/usr/bin/cat '#{workspace}/source/config/localextensions.xml' | /usr/bin/grep /custom/ | /usr/bin/cut -d "\"" -f 2 `
      sources=`/usr/bin/cat '#{workspace}/source/config/localextensions.xml' | /usr/bin/grep /custom/ | /usr/bin/cut -d '\"' -f 2 | /usr/bin/sed s/^.*custom/custom/`
      #Cheat by adding /src after new line.
      sources=sources.gsub("custom","#{workspace}/source/bin/custom")
      sources=sources.gsub("\n","/src,")
      binaries=`/usr/bin/find '#{workspace}/source/bin' -type d -name classes -print0 | /usr/bin/grep -zv /web/ | /usr/bin/grep -zv /hmc/ | /usr/bin/xargs -0 cygpath -wa`
      puts "Binaries:"
      puts binaries
      binaries=binaries.gsub("\n",",")
      tests=`/usr/bin/find '#{workspace}/source/bin/custom' -type d -name testsrc -print0 | /usr/bin/grep -zv /web/ | /usr/bin/xargs -0 cygpath -wa`
      tests=tests.gsub("\n",",")
      libraries=`/usr/bin/find '#{workspace}/source/bin' -name *.jar -print0 | /usr/bin/grep -zv /web/ | /usr/bin/grep -zv /hmc/ | /usr/bin/xargs -0 cygpath -wa`
      libraries=libraries.gsub("\n",",")
      # Nuke File Analyzer crap.
      system "rm -rf '#{workspace}'/source/log/junit/*.log.xml"
      set :sonar_junits_path, "#{workspace}/source/log/junit"
      set :sonar_jacoco_path, "#{workspace}/source/bin/platform/jacoco.exec"
      set :sonar_sources, "#{sources}"
      set :sonar_tests, "#{tests}"
      set :sonar_binaries, "#{binaries}"
      set :sonar_libraries, "#{libraries}"
      set :sonar_svn_url, "#{svn_url}"
    end

    task :install_sonar_properties do
      template = File.read("#{sonar_project_properties_template}")
      buffer = ERB.new(template).result(binding)
      dirname = File.absolute_path(File.dirname(__FILE__))
      File.open("#{dirname}/../../../../../sonar-project.properties", 'w:utf-8') {|f| f.write(buffer) }
      run_locally "/usr/bin/sed -i 's/\\\\/\\//g' '#{dirname}/../../../../../sonar-project.properties'"
    end

    task :fix_platform do
      dirname = File.absolute_path(File.dirname(__FILE__))
      run_locally "sed -i 's/$1 run/$1 $2/' '#{dirname}/../../../../../source/bin/platform/tcserver.sh';"
    end


    task :build do
      hybris.build.install_base      
      hybris.build.ant_clean
      # Ant clean nukes revision.txts!
      hybris.build.install_revisiontxt
      hybris.build.clean_packages
      #hybris.build.fix_platform
      hybris.build.ant_customize
      hybris.build.ant_build
      hybris.build.ant_package
      hybris.build.config_package
	  #hybris.build.extensions_package
      hybris.build.create_sonar_properties
      hybris.build.install_sonar_properties
      
	  
    end

	task :exploded_build do
      hybris.build.install_base      
      hybris.build.ant_clean
      # Ant clean nukes revision.txts!
      hybris.build.install_revisiontxt
      #hybris.build.clean_packages
      #hybris.build.fix_platform
      hybris.build.ant_customize
      hybris.build.ant_build
      #hybris.build.ant_package
      hybris.build.config_package
	  #hybris.build.extensions_package
      #hybris.build.create_sonar_properties
      #hybris.build.install_sonar_properties
      
	  
    end
	
	task :compile_only do
		hybris.build.install_base      
		hybris.build.ant_build  
    end
	
	task :clean_compile_only do
		hybris.build.ant_clean
		hybris.build.install_base      
		hybris.build.ant_build  
    end
	
    task :full_build do
      dirname = File.absolute_path(File.dirname(__FILE__))
      puts dirname
      #system "rm -rf '#{dirname}/../../../../../HybrisBase'"
      hybris.build.install_base
      hybris.build.ant_clean     
      # Ant clean nukes revision.txts!
      hybris.build.install_revisiontxt
      hybris.build.clean_packages
      hybris.build.fix_platform
      hybris.build.ant_customize
      hybris.build.ant_build
      hybris.build.ant_package
      hybris.build.config_package
      #hybris.build.extensions_package
      #hybris.build.platform_package
      #hybris.build.create_sonar_properties
      #hybris.build.install_sonar_properties
    end



  end
end
