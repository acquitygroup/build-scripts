desc "deploy to az dev environment"
task :azdev do
  set :stage_name, "azdev"
  set :site_name, "azdev"
  set :user, "jryan"
  set :password, "Dartfrog6"
  set :deploy_to, "/apps/hybris/belkin/hybris"
  role :app, "10.6.3.30"
end


desc "deploy to Belkin Dev environment"
task :belkindev do
  set :stage_name, "belkindev"
  set :site_name, "la-web-dev"
  #set :user, "jeff.ryan"
  #set :password, "belkin123"
  set :deploy_to, "/cygdrive/d/hybris-temp"
  set :static_media, "/cygdrive/d/hybris_static/belkinstorefront"
  set :hybris_media, "/hybris/bin/custom/belkin/belkinstorefront/web/webroot/_ui"
  set :database_ip, "172.24.8.81"
  set :database_user, "backupagent"
  set :database_pwd, "password12345"
  set :database_backupJob, "maintain_backup_HybrisDev.db"
  set :content_cronjob, "ContentExport"
  role :app, "la-web-dev"
end



desc "deploy to QA2 environment"
task :belkinqa2 do
  set :stage_name, "belkinqa2"
  set :site_name, "la-web-qa-app2"
  #set :user, "jeff.ryan"
  #set :password, "belkin123"
  set :deploy_to, "/cygdrive/d/hybris-temp"
  set :static_media, "/cygdrive/d/hybris_static/belkinstorefront"
  set :hybris_media, "/hybris/bin/custom/belkin/belkinstorefront/web/webroot/_ui"
  set :database_ip, "172.24.8.81"
  set :database_user, "backupagent"
  set :database_pwd, "password12345"
  set :database_backupJob, "maintain_backup_HybrisQA2.db"
  set :content_cronjob, "ContentExport"
  set :use_exploded_dir, "true"
  role :app, "la-web-qa-app2"
end



desc "deploy to QA1 environment"
task :belkinqa1 do
  set :stage_name, "belkinqa1"
  set :site_name, "la-web-qa-app"
  #set :user, "jeff.ryan"
  #set :password, "belkin123"
  set :deploy_to, "/cygdrive/d/hybris-temp"
  set :static_media, "/cygdrive/d/hybris_static/belkinstorefront2"
  set :hybris_media, "/hybris/bin/custom/belkin/belkinstorefront/web/webroot/_ui"
  set :database_ip, "172.24.8.81"
  set :database_user, "backupagent"
  set :database_pwd, "password12345"
  set :database_backupJob, "maintain_backup_HybrisQA.db"
  set :content_cronjob, "ContentExport"
  set :use_exploded_dir, "true"
  role :app, "la-web-qa-app"
end


desc "deploy to development environment"
task :belkindev do
  set :stage_name, "belkindev"
  set :site_name, "la-web-dev"
  #set :user, "jeff.ryan"
  #set :password, "belkin123"
  set :deploy_to, "/cygdrive/d/hybris"
  set :static_media, "/cygdrive/d/hybris_static/belkinstorefront"
  set :hybris_media, "/hybris/bin/custom/belkin/belkinstorefront/web/webroot/_ui"
  #set :database_ip, "172.24.8.81"
  #set :database_user, "backupagent"
  #set :database_pwd, "password12345"
  #set :database_backupJob, "maintain_backup_HybrisQA2.db"
  #set :content_cronjob, "ContentExport"
  role :app, "la-web-dev"
end



desc "UAT Environment"
task :UAT do
  set :stage_name, "uat"
  set :site_name, "uat.acq.acq"
  #role :app, "172.26.48.58"
  #role :web, "172.26.48.58"
end

desc "deploy to prod environment"
task :prod do
  set :stage_name, "prod"

end