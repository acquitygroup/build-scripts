default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    acquity.util.detect_os_version
    update
    #restart
  end

  task :build do
    magento.build.build
  end

  task :cold do
    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    setup
    start
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end


  task :restart, :except => { :no_release => true } do
    magento.deploy.restart_magento
  end


   task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    magento.deploy.create_release_path
    magento.deploy.copy_magento    
    finalize_update
  end

 
  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update do
    magento.deploy.deploy_magento
    magento.deploy.setup_magento
    magento.deploy.verify_magento
    cleanup      
   end

  task :setup do   
    #set :user, "root"
    acquity.util.detect_os_version
    magento.configure.fix_sudoers_for_cap
    magento.configure.install_audit_tools
    magento.configure.set_hostname
    #magento.configure.install_svn_directories
    magento.configure.install_apache
    magento.configure.install_ssl_certs
    magento.configure.install_php
    if fetch(:use_new_relic, false) == true then
      magento.configure.install_newrelic
    end
    if fetch(:skip_db_install, false) == false then
      magento.configure.install_mysql      
    end      
    magento.configure.restart_mysql
    magento.configure.configure_mysql
    magento.configure.start_http_server
    magento.configure.install_magento
    #magento.configure.install_deploy_keys
    #magento.configure.install_viewvc
    #magento.configure.sync_svn_conf
    #magento.configure.install_fuse
    #magento.configure.mount_fuse
    #magento.configure.start_http_server
    #magento.configure.install_cron_scripts
    #magento.configure.install_global_hooks
    #magento.configure.install_master_symlinks
    #magento.configure.install_slave_symlinks
    #magento.configure.start_cron
  end

  task :start do
    magento.configure.start_http_server
  end


end

after 'deploy:restart', "deploy:after_restart"