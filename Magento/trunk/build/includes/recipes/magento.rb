load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/magento/configure'
load File.dirname(__FILE__) + '/magento/build'
load File.dirname(__FILE__) + '/magento/deploy'
load File.dirname(__FILE__) + '/magento/util'


namespace :magento do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 