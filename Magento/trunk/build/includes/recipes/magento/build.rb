namespace :magento do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do

  
    task :install_base do
      dirname = File.absolute_path(File.dirname(__FILE__))
      puts dirname
      system "mkdir -p '#{dirname}/../../../../../MagentoBase'"
      system "mkdir -p '#{dirname}/../../../../../Packages'"
      path = "#{dirname}/../../../../../MagentoBase/#{magento_version}"
      if File.exists?("#{path}/magento") && File.directory?("#{path}/magento")
        puts "Base Magento Install Exists"
      else
        puts "Copying Base Magento Install"
        system "mkdir -p '#{path}'"
        #set(:acquity_username) { Capistrano::CLI.ui.ask("Acquity username: ") }
        #set(:acquity_password) { Capistrano::CLI.password_prompt("Acquity password: ") }   
        if (magento_edition == "enterprise")
          system "smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"Magento/enterprise\";get magento-enterprise-#{magento_version}.zip \"#{path}/magento-enterprise-#{magento_version}.tar.gz\";exit' "
          system "cd '#{path}'; tar -zxvf magento-enterprise-#{magento_version}.tar.gz"
          system "rm -f '#{path}/magento-enterprise-#{magento_version}.tar.gz'"
        else
          system "smbclient //srv-ag2/software/ #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"Magento/community\";get magento-#{magento_version}.zip \"#{path}/magento-#{magento_version}.tar.gz\";exit' "
          system "cd '#{path}'; tar -zxvf magento-#{magento_version}.tar.gz"
          system "rm -f '#{path}/magento-#{magento_version}.tar.gz'"
        end
       
      end
      #system "cp -Rnv '#{path}/hybris/bin' '#{dirname}/../../../../' "
      #puts "mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      #system "[ -d '#{dirname}/../../../../config' ] && mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      #puts "cd '#{dirname}/../../../../bin/platform'; source setantenv.sh ; ant server -Dinput.template=develop"
      #system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant server -Dinput.template=develop"
      #puts "[ -d '#{dirname}/../../../../config-temp' ] && cp -Rnv '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      #system "[ -d '#{dirname}/../../../../config-temp' ] && cp -Rnv  '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      #system "[ -d '#{dirname}/../../../../config-temp' ] && rm -rf '#{dirname}/../../../../config' "
      #system "[ -d '#{dirname}/../../../../config-temp' ] && mv '#{dirname}/../../../../config-temp' '#{dirname}/../../../../config' "
    end
    
    
    task :build do      
      magento.build.install_revisiontxt
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "mkdir -p '#{dirname}/../../../../Packages'"      
      system "cd '#{dirname}/../../../../sources'; rm -f '#{dirname}/../../../../Packages/magento.zip'; zip -r -x='.svn' '#{dirname}/../../../../Packages/magento.zip' *"
    end

    task :install_revisiontxt do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "rm -f '#{dirname}/../../../../source/revision.txt'"
      system "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../app'/revision.txt"            
    end
    
  end
end