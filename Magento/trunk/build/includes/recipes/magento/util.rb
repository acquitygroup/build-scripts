namespace :magento do


  namespace :util do
   
    task :fetch_logs, :roles => :app do
      if fetch(:use_vpn, false) == true then
        acquity.util.start_vpn
      end
      print "Fetching Logs from #{stage_name}.\n"
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "mkdir -p '#{dirname}/../../../../../Logs/#{stage_name}'"
      download("#{deploy_to}/log","#{dirname}/../../../../../Logs/#{stage_name}/$CAPISTRANO:HOST$/",:via=> :scp, :recursive => true)
    end
  
    task :hot_deploy, :roles => :app do
      if fetch(:use_vpn, false) == true then
        acquity.util.start_vpn
      end
      system "mkdir -p '#{dirname}/../../../../../Hot/'"
      
    end

  end
end