load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/maestro/configure'
load File.dirname(__FILE__) + '/maestro/build'
load File.dirname(__FILE__) + '/maestro/deploy'
load File.dirname(__FILE__) + '/maestro/util'
set :tomcat_version, "7.0.27"

namespace :maestro do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 