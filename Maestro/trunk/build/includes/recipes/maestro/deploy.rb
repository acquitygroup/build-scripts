namespace :maestro do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  namespace :deploy do

    task :sync_config do
      run "rm -rf /tmp/esb; mkdir -p /tmp/esb;"
      run "cd /tmp/esb; svn checkout --non-interactive --username=#{acquity_username} --password=#{acquity_password} http://subversion.acq/ondemand/#{client_shortcode}/#{project_code}/maestro/trunk/app "
      run "rsync -rav --delete --exclude \"*.svn*\" /drive1/applications/CARBON_HOME/repository/deployment/ /tmp/esb/app/repository/deployment/"  
      run "cd /tmp/esb/app; svn status | grep \\! | awk '{$1=\"\"; print \"\\\"\"$0\"\\\"\"}' | sed -e 's/^\" */\"/' | xargs svn delete; true;"
      run "cd /tmp/esb/app; svn add --force --non-interactive --username=#{acquity_username} --password=#{acquity_password} ./*"
      timevar = Time.now
      run "cd /tmp/esb/app; svn commit --non-interactive --username=#{acquity_username} --password=#{acquity_password} -m \"Dev Sync #{timevar}\""
    end
    
    #modified from hybris
    task :fix_packages do
      dirname = File.absolute_path(File.dirname(__FILE__))
      run_locally "cd '#{dirname}/../../../../Packages'; mv ./*/*/* .; find . -type d ! -name . -prune -print0 | xargs -0 rm -rf"
    end

    
    task :create_release_path, :roles => :app do
      run "mkdir -p #{release_path}"
    end

    task :detect_java_home, :roles => :app do
      if remote_file_exists?("/usr/applications/java/current") then
        java_home = "/usr/applications/java/current"
      elsif remote_file_exists?("/usr/java/latest") then
        java_home = "/usr/java/latest"
      else
        java_home = "/usr/applications/#{jdk_version}"
      end
      set :java_home, "#{java_home}"
      set :java_home_cmd, "export JAVA_HOME='#{java_home}'"
    end
    
    
    task :copy_maestro, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      upload "#{dirname}/../../../../Packages/Maestro.zip", "#{deploy_to}/Maestro.zip"
    end


    task :deploy_maestro, :roles => :app do
      run "unzip -o '#{deploy_to}/Maestro.zip' -d #{release_path}/"
    end

    task :setup_maestro, :roles => :app do
       run "rsync -rav --delete '#{release_path}/repository/deployment/' '#{deploy_to}/repository/deployment/'"
       run "rm -f #{deploy_to}/revision.txt; cp #{release_path}/revision.txt #{deploy_to}/revision.txt"
    end

    task :verify_maestro, :roles => :app do       
       run "cat #{deploy_to}/revision.txt"
    end
    
    task :stop_maestro, :roles => :app, :on_error => :continue do
      #run "cd '#{deploy_to}/bin'; #{java_home_cmd}; ./tcserver.sh #{stage_name} stop"
      run "cd '#{deploy_to}/bin'; ./wso2server.sh stop"
    end

    task :restart_maestro, :roles => :app do
      
      maestro.deploy.stop_maestro
      print "Sleeping 60 seconds.\n"
      sleep 60
      #run "cd '#{deploy_to}/current/maestro/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} start"
      run "cd '#{deploy_to}/bin'; ./wso2server.sh start"
    end

        
  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
