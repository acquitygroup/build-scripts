namespace :maestro do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do

  
    
    task :build do
      
      maestro.build.install_revisiontxt
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "mkdir -p '#{dirname}/../../../../Packages'"      
      system "cd '#{dirname}/../../../../app'; rm -f '#{dirname}/../../../../Packages/Maestro.zip'; zip -r -x='.svn' '#{dirname}/../../../../Packages/Maestro.zip' *"
    end

    task :install_revisiontxt do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "rm -f '#{dirname}/../../../../app/revision.txt'"
      system "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../app'/revision.txt"            
    end
    
  end
end