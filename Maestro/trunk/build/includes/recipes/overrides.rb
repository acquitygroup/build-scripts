default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    update
    #restart
  end

  task :build do
    maestro.build.build
  end

  task :cold do
    setup
    start
  end


  task :restart, :except => { :no_release => true } do
    maestro.deploy.restart_maestro
  end


   task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    maestro.deploy.fix_packages
    maestro.deploy.create_release_path
    maestro.deploy.copy_maestro    
    finalize_update
  end

 
  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update do
    maestro.deploy.deploy_maestro
    maestro.deploy.setup_maestro
    maestro.deploy.verify_maestro
    cleanup      
   end

  task :setup do
    set :user, "root"
    #maestro.configure.fix_sudoers_for_cap
    #maestro.configure.install_audit_tools
    #maestro.configure.set_hostname
    #maestro.configure.install_svn_directories
    #maestro.configure.install_apache
    #maestro.configure.install_maestro
    #maestro.configure.install_deploy_keys
    #maestro.configure.install_viewvc
    #maestro.configure.sync_svn_conf
    #maestro.configure.install_fuse
    #maestro.configure.mount_fuse
    #maestro.configure.start_http_server
    #maestro.configure.install_cron_scripts
    #maestro.configure.install_global_hooks
    #maestro.configure.install_master_symlinks
    #maestro.configure.install_slave_symlinks
    #maestro.configure.start_cron
  end

  task :start do
    maestro.configure.start_http_server
  end


end

after 'deploy:restart', "deploy:after_restart"