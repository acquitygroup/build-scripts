namespace :nodejs do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do

  
    
    task :build do
      
      
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "mkdir -p '#{dirname}/../../../../content/dist'" 
      run_locally "cd '#{dirname}/../../../../'; rm -f '#{dirname}/../../../../content/dist/*'"
      run_locally "cd '#{dirname}/../../../../content'; npm install"
      system "cd '#{dirname}/../../../../content'; grunt build --force --no-color"
      nodejs.build.install_revisiontxt
      run_locally "cd '#{dirname}/../../../../content/dist'; find . -name '*.zip' -exec zip {} revision.txt \\;"
    end

    task :install_revisiontxt do
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "rm -f '#{dirname}/../../../..content/dist/revision.txt'"
      run_locally "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../content/dist'/revision.txt"
      if ENV['BUILD_TAG'] then
        build_tag = ENV['BUILD_TAG']
      else 
        build_tag = "No build tag parameter found"
      end
      File.open("#{dirname}/../../../../content/dist/revision.txt", 'a') { |f2|  f2.puts "Jenkins Build Tag: #{build_tag}" }
      
    end
    
  end
end