namespace :nodejs do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  namespace :deploy do

    task :sync_config do
      run "rm -rf /tmp/esb; mkdir -p /tmp/esb;"
      run "cd /tmp/esb; svn checkout --non-interactive --username=#{acquity_username} --password=#{acquity_password} http://subversion.acq/ondemand/#{client_shortcode}/#{project_code}/nodejs/trunk/app "
      run "rsync -rav --delete --exclude \"*.svn*\" /drive1/applications/CARBON_HOME/repository/deployment/ /tmp/esb/app/repository/deployment/"  
      run "cd /tmp/esb/app; svn status | grep \\! | awk '{$1=\"\"; print \"\\\"\"$0\"\\\"\"}' | sed -e 's/^\" */\"/' | xargs svn delete; true;"
      run "cd /tmp/esb/app; svn add --force --non-interactive --username=#{acquity_username} --password=#{acquity_password} ./*"
      timevar = Time.now
      run "cd /tmp/esb/app; svn commit --non-interactive --username=#{acquity_username} --password=#{acquity_password} -m \"Dev Sync #{timevar}\""
    end
    
    task :create_release_path, :roles => :app do
      run "mkdir -p #{release_path}"
    end

    
    task :verify_down, :roles => :app do   
      flag = false
      find_servers(:roles => :app).each do |server|
        server_status = capture "cat '#{deploy_to}'/status.txt", :hosts => server.host
        puts "Status: #{server_status}"
        if server_status.include? "DOWN"
          flag = true;
        end
      end
      if flag
        puts "Down Server(s) Found OK..."        
      else
        raise "No Down Server(s) Found!"
      end
    end
      
    task :flip_status, :roles => :app do
      nodejs.deploy.verify_down
      run "if grep -Fxq \"DOWN\" '#{deploy_to}'/status.txt; then { rm '#{deploy_to}'/status.txt; echo \"UP\" > '#{deploy_to}'/status.txt; } else { rm '#{deploy_to}'/status.txt; echo \"DOWN\" > '#{deploy_to}'/status.txt; } fi "    
    end
    
    task :copy_nodejs, :roles => :app do
      dirname = File.absolute_path(File.dirname(__FILE__))
      if File.exists?("#{dirname}/../../../../content/dist/node-content-#{stage_name}.zip")
        upload "#{dirname}/../../../../content/dist/node-content-#{stage_name}.zip", "#{deploy_to}/node-content.zip"
      else
        upload "#{dirname}/../../../../content/dist/node-content.zip", "#{deploy_to}/node-content.zip"
      end
    end


    task :deploy_nodejs, :roles => :app do
      run "unzip -o '#{deploy_to}/node-content.zip' -d #{release_path}/"
    end

    task :setup_nodejs, :roles => :app do
      run "rsync -rav --delete '#{release_path}/repository/deployment/' '#{deploy_to}/repository/deployment/'"
      run "rm -f #{deploy_to}/revision.txt; cp #{release_path}/revision.txt #{deploy_to}/revision.txt"
    end

    task :verify_nodejs, :roles => :app do       
      run "cat #{release_path}/revision.txt"
    end
    
    task :stop_nodejs, :roles => :app, :on_error => :continue do
      #run "cd '#{deploy_to}/bin'; #{java_home_cmd}; ./tcserver.sh #{stage_name} stop"
      run "#{sudo} service httpd stop"
    end

    task :restart_nodejs, :roles => :app do
      
      #Cut readd when sudoers works right
      #run "#{sudo} service httpd restart"
    end

        
  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
