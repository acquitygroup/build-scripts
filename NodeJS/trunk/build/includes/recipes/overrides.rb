default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    update
    #restart
  end

  task :build do
    nodejs.build.build
  end

  task :cold do
    setup
    start
  end


  task :restart, :except => { :no_release => true } do
    nodejs.deploy.restart_nodejs
  end


   task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    nodejs.deploy.create_release_path
    nodejs.deploy.copy_nodejs 
    finalize_update
  end
  
  task :flip do
     if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    nodejs.deploy.flip_status
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :verify_flip do
     if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    nodejs.deploy.verify_down
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end

 
  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update do
    nodejs.deploy.deploy_nodejs
    #nodejs.deploy.setup_nodejs
    nodejs.deploy.verify_nodejs
    cleanup      
   end

  task :setup do
    set :user, "root"
    #nodejs.configure.fix_sudoers_for_cap
    #nodejs.configure.install_audit_tools
    #nodejs.configure.set_hostname
    #nodejs.configure.install_svn_directories
    #nodejs.configure.install_apache
    #nodejs.configure.install_nodejs
    #nodejs.configure.install_deploy_keys
    #nodejs.configure.install_viewvc
    #nodejs.configure.sync_svn_conf
    #nodejs.configure.install_fuse
    #nodejs.configure.mount_fuse
    #nodejs.configure.start_http_server
    #nodejs.configure.install_cron_scripts
    #nodejs.configure.install_global_hooks
    #nodejs.configure.install_master_symlinks
    #nodejs.configure.install_slave_symlinks
    #nodejs.configure.start_cron
  end

  task :start do
    nodejs.configure.start_http_server
  end


end

after 'deploy:restart', "deploy:after_restart"