load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/nodejs/configure'
load File.dirname(__FILE__) + '/nodejs/build'
load File.dirname(__FILE__) + '/nodejs/deploy'
load File.dirname(__FILE__) + '/nodejs/util'
set :tomcat_version, "7.0.27"

namespace :nodejs do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 