default_run_options[:pty] = false
default_run_options[:shell] = false 

namespace :deploy do 
 
  task :default, :roles => :app do
    update
    #restart
  end

  task :build do
    content.build.build
  end

  task :cold do
    setup
    start
  end


  task :restart, :except => { :no_release => true } do
    content.deploy.restart_content
  end


   task :update_code, :except => { :no_release => true }  do
    on_rollback { restart }

    if fetch(:use_vpn, false) == true then
      acquity.util.start_vpn
    end
    content.deploy.create_release_path
    content.deploy.copy_content 
    finalize_update
  end

 
  task :after_restart do
    if fetch(:use_vpn, false) == true then
      acquity.util.stop_vpn
    end
  end
  
   task :finalize_update do
    content.deploy.deploy_content
    #content.deploy.setup_content
    content.deploy.verify_content
    cleanup      
   end

  task :setup do
    set :user, "root"
    #content.configure.fix_sudoers_for_cap
    #content.configure.install_audit_tools
    #content.configure.set_hostname
    #content.configure.install_svn_directories
    #content.configure.install_apache
    #content.configure.install_content
    #content.configure.install_deploy_keys
    #content.configure.install_viewvc
    #content.configure.sync_svn_conf
    #content.configure.install_fuse
    #content.configure.mount_fuse
    #content.configure.start_http_server
    #content.configure.install_cron_scripts
    #content.configure.install_global_hooks
    #content.configure.install_master_symlinks
    #content.configure.install_slave_symlinks
    #content.configure.start_cron
  end

  task :start do
    content.configure.start_http_server
  end


end

after 'deploy:restart', "deploy:after_restart"