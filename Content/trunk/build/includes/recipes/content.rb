load File.dirname(__FILE__) + '/acquity/util'
load File.dirname(__FILE__) + '/content/configure'
load File.dirname(__FILE__) + '/content/build'
load File.dirname(__FILE__) + '/content/deploy'
load File.dirname(__FILE__) + '/content/util'
set :tomcat_version, "7.0.27"

namespace :content do
  task :setup, roles => :web, :except => { :no_release => true } do
   
  end

  task :symlink, roles => :web, :except => { :no_release => true } do
 
  end

  task :update_code, roles => :web, :except => { :no_release => true } do
   
  end


  task :install_shared_symlinks, roles => :web, :except => { :no_release => true } do
  
  end

end 