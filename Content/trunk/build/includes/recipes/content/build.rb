namespace :content do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do

  
    
    task :build do
      
      content.build.install_revisiontxt
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "mkdir -p '#{dirname}/../../../../Packages'"      
      system "cd '#{dirname}/../../../../content'; rm -f '#{dirname}/../../../../Packages/Content.zip'; zip -r -x='.svn' '#{dirname}/../../../../Packages/Content.zip' *"
    end

    task :install_revisiontxt do
      dirname = File.absolute_path(File.dirname(__FILE__))
      system "rm -f '#{dirname}/../../../../content/revision.txt'"
      system "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../content'/revision.txt"            
    end
    
  end
end